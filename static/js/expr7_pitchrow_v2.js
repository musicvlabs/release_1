(function(window) {

// global vars..
var current_notes = [],
    tone_elems = [],
    drone_elem,
    path = '/static/stimuli/instruments',
    note_array = ["c3","cs3","d3","ds3","e3","f3","fs3","g3","gs3","a3","as3","b3","c4"],
    note_names = ["Sa","re","Re","ga","Ga","Ma","Ma#","Pa","dha","Dha","ni","Ni","Sa1"],
    levels = {
      "1": {rows: 3, notes: [0, 2, 4, 7, 9]},
      "2": {rows: 3, notes: [0, 2, 4, 5, 7, 9, 11]},
      "3": {rows: 4, notes: [0, 2, 4, 5, 7, 9, 11]},
      "4": {rows: 3, notes: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]},
      "5": {rows: 4, notes: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]}
    },
    playing = false,
    correct = 0,
    incorrect = 0;


// initialize stuff..
function init() {
  // append audio elements
  tone_elems = [
    document.createElement('audio'),
    document.createElement('audio'),
    document.createElement('audio'),
    document.createElement('audio')
  ];
  drone_elem = document.createElement('audio');
  drone_elem.setAttribute('src', '/static/drones/3.mp3');

  //console.log('initing');


  // attach flip panel click handlers
  // shows corresponding panel when clicked on level selector
  $(".flip").click(function(e) {
    e.preventDefault();
    // add the active class to li
    $('#level-selector li').removeClass("active");
    $(this).parent().addClass("active");

    // get the target panel
    var target = $(this).attr("href");
    // get level
    var level = target.split('panel')[1];
    // render panel
    render_panel(level, target);
    // show it!
    $(".panel").not(target).hide();
    $(target).slideDown("fast");
    return false;
  });

  ML.collectRetentionAndSave(6);
}

// render the radio choice panel based on the level
function render_panel(level, target) {
  $('.panel').html('');
  var panel_html = $('#panel-template').html();
  $(target).html(panel_html);
  var level_html = $('#level-template').html();
  for(var i = 0; i < levels[level]['rows']; i++) {
    $(target).append(level_html);
  }
  $(target).find('input[name="swar"]').parent().hide();
  var notes = levels[level]['notes'];
  for(var idx in notes) {
    var note_name = note_names[notes[idx]];
    $(target).find('input[value="' + note_name + '"]').parent().show();
  }
  $(target).append($('#submit-btn-template').html());
}

// handler when play tone btn clicked
function playtone_clicked() {
   var instrument= $('#instrument option:selected').val();
   var level = $('#level-selector li.active').val();
   //console.log(level, instrument);
   play_tone(level, instrument);
}

// generic function to play a tone based on level and instrument
function play_tone(level, instrument) {
  //var currentnote_path;
  // if user has not submitted, play the current note
  if(playing === true) {
    var i = 0;
    (function iter() {
      //console.log(current_notes[i]);
      play(i, instrument);

      if(++i < current_notes.length) {
        setTimeout(iter, 2000);
      }
    })();
  }
  // else generate a new random note; and that becomes the current note
  else {
    current_notes = get_random_notes(level);
    playing = true;
    var i = 0;
    (function iter() {
      //console.log(current_notes[i]);
      play(i, instrument);

      if(++i < current_notes.length) {
        setTimeout(iter, 2000);
      }
    })();
  }
}

function play(i, instrument) {
    //console.log('iter', i);
    var currentnote_path = path + '/' + instrument + '/' +
      current_notes[i] + '.mp3';

    //console.log('playing', currentnote_path, i, tone_elems[i]);
    tone_elems[i].setAttribute('src', currentnote_path);
    tone_elems[i].play();
}

// return a random note based on the level given..
function get_random_notes(level) {
  //console.log('got level', level);
  var level_array = levels[level]['notes'];
  //console.log('level array', level_array);
  var notes = [];
  for(var i = 0; i < levels[level]['rows']; i++) {
    var noteno = level_array[Math.floor(Math.random() * level_array.length)];
    //console.log('note no', noteno);
    notes.push(note_array[noteno]);
  }
  //return note_array[noteno];
  //console.log(notes);
  return notes;
}

function submit_clicked() {
  var level = $('#level-selector li.active').val();
  var user_ip = [];
  var target = $('#level-selector li.active a').attr('href');
  $(target).find('form[name="swar-choices"] input:checked').each(function() {
    user_ip.push($(this).val());
  });
  //console.log(user_ip);
  check_submit(user_ip, level);
}

function check_submit(user_ip, level) {
  // if user is not currently playing(meaning previously submitted the same
  // input); simply return; don't evaluate..
  if(playing == false) {
    return;
  }

  for(var i in user_ip) {
    var all_correct = false;
    var idx = note_names.indexOf(user_ip[i]);

    if(note_array[idx] == current_notes[i]) {
      all_correct = true;
    }
    else {
      all_correct = false;
    }
  }

  if(all_correct) {
    $('#answer').html('Right');
    //var right = document.getElementById("div1-1").innerHTML = ('Right');
    correct = correct + 1;
    ML.postScore(1, 6, level);
    // set playing to false only if the user has got it right..
    // i.e no retrying next time..
    playing = false;
  }
  else {
    $('#answer').html('Wrong');
    //var wrong = document.getElementById("div1-1").innerHTML = ('Wrong');
    incorrect = incorrect + 1;
    ML.postScore(0, 6, level);
  }

  var score = correct + incorrect;
  $('#score').html('Your score is ' + correct + ' out of ' + score);
  //document.getElementById("div1-2").innerHTML=("Your score is" + '&nbsp;' + window.correct + '&nbsp;' + "out of" + '&nbsp;' + r);
}

function play_tonic() {
  drone_elem.play();
}

function stop_tonic() {
  drone_elem.pause();
}

// expose public methods..
window.init = init;
window.playtone_clicked = playtone_clicked;
window.submit_clicked = submit_clicked;
window.play_tonic = play_tonic;
window.stop_tonic = stop_tonic;

})(window);
