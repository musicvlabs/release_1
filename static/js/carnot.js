var LOG_LEVEL = 1;
org = typeof(org) === 'undefined' ? {} : org;
org.sriku = org.sriku || {};
org.sriku.Carnot = (function (Carnot) {
    var GLOBAL = this;
var validEventName = (function () {
    var dummy = {};
    return function (eventName) {
        ;
        if (dummy[eventName]) {
            throw new Error('Invalid event name - ' + eventName);
        }
        return eventName;
    };
}());
var nextEventableWatcherID = 1;
function Eventable(obj) {
    var watchers = {};
    function on(eventName, watcher) {
        ;
        ;
        var i, N;
        eventName = validEventName(eventName);
        var eventWatchers = watchers[eventName] || (watchers[eventName] = {});
        var id = watcher['__steller_eventable_id__'] || 0;
        if (id in eventWatchers) {
            return this;
        }
        if (!id) {
            Object.defineProperty(watcher, '__steller_eventable_id__', {
                value: (id = nextEventableWatcherID++),
                enumerable: false,
                configurable: false,
                writable: false
            });
        }
        eventWatchers[id] = watcher;
        return this;
    }
    function off(eventName, watcher) {
        ;
        var i, N;
        eventName = validEventName(eventName);
        var eventWatchers = watchers[eventName];
        if (!eventWatchers) {
            return this;
        }
        var wid = (watcher && watcher['__steller_eventable_id__']) || 0;
        if (wid) {
            ;
            delete eventWatchers[wid];
        } else if (!watcher) {
            delete watchers[eventName];
        }
        return this;
    }
    function emit(eventName) {
        eventName = validEventName(eventName);
        var eventWatchers = watchers[eventName];
        if (!eventWatchers) {
            return this;
        }
        for (var id in eventWatchers) {
            try {
                eventWatchers[id].apply(this, arguments);
            } catch (e) {
                do { if (1 <= LOG_LEVEL) { console.log("eventable.js" + '[' + 107 + ']:\t', "Exception in event watcher - ", e); } } while (false);
            }
        }
        return this;
    }
    ;
    ;
    ;
    obj.on = on;
    obj.off = off;
    obj.emit = emit;
    return obj;
}
Eventable.observe = function (obj, methodName, eventName) {
    ;
    eventName = validEventName(eventName || methodName);
    var method = obj[methodName];
    ;
    obj[methodName] = function () {
        var result = method.apply(this, arguments);
        var argv = Array.prototype.slice.call(arguments);
        argv.unshift(eventName);
        this.emit.apply(this, argv);
        return result;
    };
    return obj;
};
var kAsyncEventableKey = '__steller_async_eventable__';
function AsyncEventable(obj) {
    obj = Eventable(obj);
    var on = obj.on;
    obj.on = function asyncOn(eventName, watcher) {
        ;
        ;
        if (!watcher) {
            return this;
        }
        var async = watcher[kAsyncEventableKey];
        if (!async) {
            Object.defineProperty(watcher, kAsyncEventableKey, {
                value: (async = function () {
                    var argv = arguments;
                    setTimeout(function () { watcher.apply(obj, argv); }, 0);
                }),
                enumerable: false,
                configurable: false,
                writable: false
            });
        }
        on(eventName, async);
    };
    return obj;
}
    Carnot = Eventable(Carnot);
var kSvarasthanaREStr = '([SrRgGmMPdDnN][\\+\\-]*)|[,_]';
var kSvarasthanaTokenREStr = '(' + kSvarasthanaREStr + ')+';
var kSvarasthanaLineTokenRE = new RegExp('^' + kSvarasthanaTokenREStr + '$');
function Parse(pre) {
    function trim(s) {
        return s.trim();
    }
    function isNotEmpty(s) {
        return s.length > 0;
    }
    function split(str, re) {
        return str.split(re).map(trim).filter(isNotEmpty);
    }
    function normalize(str) {
        return str.trim().replace(/\s+/g, ' ');
    }
    function normalizePropertyKey(key) {
        return normalize(key).toLowerCase();
    }
    function parseLine(line) {
        var type, tokens;
        if (/^\>/.test(line)) {
            type = 'text';
            tokens = [line.substring(1).trim()];
        } else if (/\=/.test(line)) {
            type = 'property';
            tokens = line.split('=').filter(isNotEmpty).map(normalize);
            ;
            tokens[0] = normalizePropertyKey(tokens[0]);
        } else {
            tokens = split(line, /\s/g);
            if (tokens.filter(function (tok) {
                return kSvarasthanaLineTokenRE.test(tok);
            }).length === tokens.length) {
                type = 'svarasthana';
            } else {
                type = 'lyrics';
            }
        }
        return {
            type: type,
            tokens: tokens
        };
    }
    var properties = {
        '$aksharas per line': 16,
        '$tala pattern' : ',',
        '$stretch' : 1.0
    };
    function copyProps(src, dest) {
        var k;
        for (k in src) {
            if (src.hasOwnProperty(k)) {
                dest[k] = src[k];
            }
        }
        return dest;
    }
    function parseProperties(lineObjects) {
        var props = copyProps(properties, {});
        var filteredLines = [];
        lineObjects.forEach(function (lineObj) {
            if (lineObj.type === 'property' && lineObj.tokens.length === 2) {
                props['$'+lineObj.tokens[0]] = lineObj.tokens[1];
            } else {
                filteredLines.push(lineObj);
            }
        });
        if (filteredLines.length === 0) {
            copyProps(props, properties);
            return null;
        }
        return {
            type: 'paragraph',
            lines: filteredLines,
            properties: props
        };
    }
    var code = pre.textContent.trim();
    var paragraphs = split(code, /([ \t]*\n)([ \t]*\n)+/);
    paragraphs = paragraphs.map(function (p) {
        var lines = split(p, /\n/).map(parseLine);
        return parseProperties(lines);
    }).filter(function (l) { return l; });
    if (paragraphs.length === 0) {
        return [{type: 'properties', properties: properties}];
    }
    return paragraphs;
}
var kSvarasthanaTokenGlobalSearchRE = new RegExp(kSvarasthanaREStr, "g");
var kPhraseGroupingsRE = /([_]|\([_]+\))+/;
var kSvaraOctaveSuffix = {
    overdot: String.fromCharCode(parseInt("0307", 16)),
    overddot: String.fromCharCode(parseInt("0308", 16)),
    underdot: String.fromCharCode(parseInt("0323", 16)),
    underddot: String.fromCharCode(parseInt("0324", 16))
};
function RenderSVG(window, paragraphs, style) {
	console.log('in rendersvg');
    var keyTalaPattern = '$tala pattern';
    var keyAksharasPerLine = '$aksharas per line';
    var keyPhrases = '$phrases';
    var keyAksharas = '$aksharas';
    var keyLineSpacing = '$line spacing';
    var keyParaSpacing = '$para spacing';
    var keyNotationFontSize = '$notation font size';
    var keyNotationSmallFontSize = '$notation small font size';
    var keyNotationFont = '$notation font';
    var keyTextFont = '$text font';
    var keyStretch = '$stretch';
    var keyStretchSpace = '$stretch space';
    var keyMarginTop = '$margin top';
    var keyMarginLeft = '$margin left';
    var keyLineExtensionTop = '$line extension top';
    var keyLineExtensionBottom = '$line extension bottom';
    var talaCache = {};
    var cursor = {x: 0, y: 0, xmax: 0, ymax: 0};
    var pendingLines = {};
    loadStyleDefaults(style);
    cursor.y = style[keyMarginTop];
    cursor.x = style[keyMarginLeft];
    processTalaPatterns(paragraphs);
    var div = window.document.createElement('div');
    var svgns = 'http://www.w3.org/2000/svg';
    var svg;
    var commitSVG = function (makeNew) {
        if (svg) {
            svg.setAttribute('width', cursor.xmax);
            svg.setAttribute('height', cursor.ymax);
            div.style.width = ''+cursor.xmax+'px';
        }
        if (makeNew) {
            svg = window.document.createElementNS(svgns, 'svg');
            div.appendChild(svg);
            cursor.y = style[keyMarginTop];
            cursor.x = style[keyMarginLeft];
            cursor.xmax = 0;
            cursor.ymax = 0;
        }
    };
    commitSVG(true);
    paragraphs.forEach(typesetPara);
    function extendLine(ix, x, fromY, toY) {
        var l = pendingLines[ix];
        if (!l) {
            l = pendingLines[ix] = {x: x, fromY: fromY, toY: toY};
        }
        l.toY = toY;
    }
    function flushLines() {
        var ix, l;
        for (ix in pendingLines) {
            if (pendingLines.hasOwnProperty(ix)) {
                l = pendingLines[ix];
                svgelem(svg, 'line', {
                    x1: l.x,
                    y1: l.fromY,
                    x2: l.x,
                    y2: l.toY,
                    'stroke-width': 2,
                    stroke: 'black'
                });
            }
        }
        pendingLines = {};
    }
    function typesetPara(para, i) {
        if (para.lines) {
            para.lines.forEach(function (line) { typesetLine(para, line); });
            typesetPhraseGroupCurves(para);
            flushLines();
            commitSVG(i + 1 < paragraphs.length);
        }
    }
    function typesetLine(para, line) {
        switch (line.type) {
            case 'text':
                flushLines();
                typesetText(para, line);
                break;
            case 'svarasthana':
                typesetSvarasthana(para, line);
                break;
            case 'lyrics':
                typesetLyrics(para, line);
                break;
            default:
                throw new Error('Unknown line type ' + line.type);
        }
    }
    function renderPhraseGroupCurve(curve) {
        var x2 = curve.x2 - 0.5 * curve.dx;
        var ctrlx = (curve.x1 + x2) / 2;
        var ctrly = curve.y1 + Math.min(100, 5 + 0.125 * (x2 - curve.x1));
        svgelem(svg, 'path', {
            d: [
                'M' + curve.x1 + ',' + curve.y1,
                'Q' + ctrlx + ',' + ctrly,
                '' + x2 + ',' + curve.y2,
                'Q' + ctrlx + ',' + (ctrly + 3),
                '' + curve.x1 + ',' + curve.y1
            ].join(' '),
            'stroke-linejoin': 'round',
            'stroke-width': '0.5',
            stroke: 'black',
            fill: 'black'
        });
        cursor.xmax = Math.max(cursor.xmax, curve.x2);
        cursor.ymax = Math.max(cursor.ymax, ctrly);
    }
    function typesetPhraseGroupCurves(para) {
        if (!(keyPhrases in para.properties)) {
            return;
        }
        var phrasesStr = para.properties[keyPhrases].replace(/\s/g, '');
        if (!kPhraseGroupingsRE.test(phrasesStr)) {
            throw new Error('Bad phrase groupings syntax: ' + phrasesStr);
        }
        var length = phrasesStr.match(/[_]/g).length;
        var tala = para.properties[keyTalaPattern];
        var akshIx = 0, instrIx = 0, tokIx = 0, subDivIx = 0, instr;
        while (akshIx < para.tala_interval.from) {
            if (tala.instructions[instrIx].tick) {
                akshIx++;
            }
            instrIx++;
        }
        while (instrIx < tala.instructions.length && tala.instructions[instrIx].space) {
            ++instrIx;
        }
        var givenAksharas = +(para.properties[keyAksharas] || para.properties[keyAksharasPerLine]);
        var startAkshIx = akshIx;
        var startInstrIx = instrIx;
        var subdivs = length / givenAksharas;
        var stretch = (+para.properties[keyStretch]) * style[keyStretch];
        var stretchSpace = stretch * ((+para.properties[keyStretchSpace]) || style[keyStretchSpace]);
        var curves = [];
        var dy = 0.75 * style[keyLineSpacing];
        cursor.y -= dy;
        while (akshIx < para.tala_interval.to) {
            instr = tala.instructions[instrIx];
            if (instr.tick) {
                dx = instr.tick * stretch / subdivs;
                switch (phrasesStr[tokIx]) {
                    case '_':
                        cursor.x += dx;
                        ++tokIx;
                        break;
                    case '(':
                        curves.push({dx: dx, x1: cursor.x, y1: cursor.y});
                        ++tokIx;
                        continue;
                    case ')':
                        curves[curves.length-1].x2 = cursor.x;
                        curves[curves.length-1].y2 = cursor.y;
                        ++tokIx;
                        continue;
                    default:
                        ;
                }
                ++subDivIx;
                if (subDivIx >= subdivs) {
                    subDivIx = 0;
                    ++akshIx;
                    ++instrIx;
                } else {
                    continue;
                }
            }
            if (tokIx < phrasesStr.length && phrasesStr[tokIx] === ')') {
                curves[curves.length-1].x2 = cursor.x;
                curves[curves.length-1].y2 = cursor.y;
                ++tokIx;
            }
            while (instrIx < tala.instructions.length && (instr = tala.instructions[instrIx], !instr.tick)) {
                if (instr.space) {
                    if (instr.scale) {
                        cursor.x += instr.space * stretchSpace;
                    } else {
                        cursor.x += instr.space;
                    }
                } else if (instr.line) {
                    if (instr.draw) {
                        extendLine(instrIx,
                                cursor.x,
                                cursor.y - style[keyLineSpacing] - style[keyLineExtensionTop] + dy,
                                cursor.y + style[keyLineExtensionBottom]);
                    }
                    cursor.x += instr.line;
                }
                instr = tala.instructions[++instrIx];
            }
        }
        if (tokIx < phrasesStr.length) {
            ;
            curves[curves.length-1].x2 = cursor.x;
            curves[curves.length-1].y2 = cursor.y;
            ++tokIx;
        }
        ;
        curves.forEach(renderPhraseGroupCurve);
        cursor.y = cursor.ymax;
    }
    function typesetText(para, line) {
        svgelem(svg, 'text', {x: cursor.x, y: cursor.y, style: ('font-family: ' + style[keyTextFont] + ';') + 'font-size: 12pt;'}, line.tokens.join(' '));
        nextLine();
        cursor.y += style[keyLineExtensionTop] + style[keyLineExtensionBottom];
    }
    function typesetSvarasthana(para, line) {
        typesetTimedText(para, line);
    }
    function getSubSvaras(word) {
        return word.match(kSvarasthanaTokenGlobalSearchRE) || [];
    }
    function getSyllables(word) {
        var parts = [];
        var lastIx, i, N, c;
        for (i = 0, lastIx = 0, N = word.length; i < N; ++i) {
            c = word.charAt(i);
            if (c === ',' || c === '_') {
                if (i > lastIx) {
                    parts.push(word.substring(lastIx, i));
                }
                lastIx = i + 1;
                parts.push(c);
            }
        }
        if (i > lastIx) {
            parts.push(word.substring(lastIx, i));
        }
        return parts;
    }
    function typesetTimedText(para, line, additionalTextStyle) {
        var tala = para.properties[keyTalaPattern];
        var akshIx = 0, instrIx = 0, tokIx = 0, instr;
        while (akshIx < para.tala_interval.from) {
            if (tala.instructions[instrIx].tick) {
                akshIx++;
            }
            instrIx++;
        }
        while (instrIx < tala.instructions.length && tala.instructions[instrIx].space) {
            ++instrIx;
        }
        var givenAksharas = +(para.properties[keyAksharas] || para.properties[keyAksharasPerLine]);
        var startAkshIx = akshIx;
        var startInstrIx = instrIx;
        var subdivs = line.tokens.length / givenAksharas;
        var stretch = (+para.properties[keyStretch]) * style[keyStretch];
        var stretchSpace = stretch * ((+para.properties[keyStretchSpace]) || style[keyStretchSpace]);
        var textStyleBase = ('font-family:' + (line.type === 'lyrics' ? 'serif' : style[keyNotationFont]) + ';') + (additionalTextStyle || '');
        var notationFontSizes = [
            style[keyNotationFontSize],
            (+(para.properties[keyNotationSmallFontSize] || style[keyNotationSmallFontSize]))
            ];
        notationFontSizes.push(notationFontSizes[1] - (notationFontSizes[0] - notationFontSizes[1]));
        var fontSize = function (n) {
            return 'font-size: ' + notationFontSizes[n > 6 ? 2 : (n > 2 ? 1 : 0)] + 'pt;';
        };
        var textStyle = textStyleBase + fontSize(subdivs);
        var subDivIx = 0;
        var subsvaras, props, dx;
        var isSvarasthana = line.type === 'svarasthana';
        ;
        subdivs = Math.floor(subdivs);
        props = {x: cursor.x, y: cursor.y, style: textStyle};
        var renderSubsvara = function (s) {
            svgelem(svg, 'text', props, showSvara(s));
            props.x += dx / subsvaras.length;
        };
        var renderSubSyllable = function (s) {
            svgelem(svg, 'text', props, showSyllable(s));
            props.x += dx / subsvaras.length;
        };
        while (akshIx < para.tala_interval.to) {
            instr = tala.instructions[instrIx];
            if (instr.tick) {
                dx = instr.tick * stretch / subdivs;
                props.x = cursor.x;
                props.y = cursor.y;
                props.style = textStyle;
                if (isSvarasthana) {
                    subsvaras = getSubSvaras(line.tokens[tokIx]);
                    props.style = textStyleBase + fontSize((subsvaras.length > 2 ? subsvaras.length : 1) * subdivs);
                    subsvaras.forEach(renderSubsvara);
                } else {
                    subsvaras = getSyllables(line.tokens[tokIx]);
                    props.style = textStyleBase + fontSize(subdivs);
                    subsvaras.forEach(renderSubSyllable);
                }
                cursor.x += dx;
                ++subDivIx;
                ++tokIx;
                if (subDivIx >= subdivs) {
                    subDivIx = 0;
                    ++akshIx;
                    ++instrIx;
                } else {
                    continue;
                }
            }
            while (instrIx < tala.instructions.length && (instr = tala.instructions[instrIx], !instr.tick)) {
                if (instr.space) {
                    if (instr.scale) {
                        cursor.x += instr.space * stretchSpace;
                    } else {
                        cursor.x += instr.space;
                    }
                } else if (instr.line) {
                    if (instr.draw) {
                        extendLine(instrIx,
                                cursor.x,
                                cursor.y - style[keyLineSpacing] - style[keyLineExtensionTop],
                                cursor.y + style[keyLineExtensionBottom]);
                    }
                    cursor.x += instr.line;
                }
                instr = tala.instructions[++instrIx];
            }
        }
        nextLine();
    }
    function typesetLyrics(para, line) {
        typesetTimedText(para, line, 'font-style: italic;');
    }
    function nextLine(spacingKey) {
        cursor.y += style[spacingKey || keyLineSpacing];
        cursor.ymax = Math.max(cursor.y, cursor.ymax);
        cursor.xmax = Math.max(cursor.x + 40, cursor.xmax);
        cursor.x = style[keyMarginLeft];
    }
    function processTalaPatterns(paragraphs) {
        var fromAkshara = 0, toAkshara = 0, aksharas = 0, aksharasInTala = 0, aksharasPerLine = 0;
        var i, N, para, tala, aksh;
        for (i = 0, N = paragraphs.length; i < N; ++i) {
            para = paragraphs[i];
            tala = para.properties[keyTalaPattern];
            aksharasInTala = countAksharasInTala(tala);
            aksharasPerLine = +(para.properties[keyAksharasPerLine]);
            aksharas = +(para.properties[keyAksharas] || para.properties[keyAksharasPerLine]);
            toAkshara += aksharas;
            aksh = aksharasInTala;
            while (toAkshara > aksh) {
                para.properties[keyTalaPattern] += tala;
                aksh += aksharasInTala;
            }
            tala = para.properties[keyTalaPattern] = compileTala(para.properties[keyTalaPattern].replace(/\|\|+/g, '||'));
            var paraAksharas = +(para.properties[keyAksharas] || para.properties[keyAksharasPerLine]);
            para.lines.forEach(function (line) {
                if (line.type !== 'text') {
                    if (line.tokens.length < paraAksharas) {
                        var i, j, M, E, N, newTokens = [];
                        M = Math.floor(aksh / line.tokens.length);
                        E = Math.round(aksh - M * line.tokens.length);
                        for (i = 0, N = line.tokens.length; i < N; ++i) {
                            newTokens.push(line.tokens[i]);
                            for (j = 1; j < M; ++j) {
                                newTokens.push('_');
                            }
                        }
                        for (i = 0; i < E; ++i) {
                            newTokens.push('_');
                        }
                        line.tokens = newTokens;
                    }
                }
            });
            para.tala_interval = {
                from: fromAkshara,
                to: toAkshara
            };
            fromAkshara = toAkshara;
            while (fromAkshara >= aksharasInTala) {
                fromAkshara -= aksharasInTala;
                toAkshara -= aksharasInTala;
            }
        }
        return paragraphs;
    }
    function compileTala(talaPattern) {
        var talaKey = '$' + talaPattern;
        if (talaKey in talaCache) {
            return talaCache[talaKey];
        }
        var i, N, c, instrs = [], nearLine;
        for (i = 0, N = talaPattern.length; i < N; ++i) {
            c = talaPattern.charAt(i);
            nearLine = (i + 1 < N && talaPattern.charAt(i+1) === '|') || (i > 0 && talaPattern.charAt(i-1) === '|');
            switch (c) {
                case '|' :
                    if (nearLine) {
                        instrs.push({line: 5, draw: true});
                    } else {
                        instrs.push({line: 10, draw: true});
                    }
                    break;
                case ' ':
                    if (nearLine) {
                        instrs.push({space: 15, scale: false});
                    } else {
                        instrs.push({space: 30, scale: true});
                    }
                    break;
                case ',':
                    instrs.push({tick: 40});
                    break;
                case '_':
                    instrs.push({line: 10, draw: false});
                    break;
                default:
                    throw new Error('Unknown pattern character [' + c + '] in tala - "' + talaPattern + '"');
            }
        }
        return (talaCache[talaKey] = {
            pattern: talaPattern,
            aksharas: countAksharasInTala(talaPattern),
            instructions: instrs
        });
    }
    function countAksharasInTala(talaPattern) {
        var commas = talaPattern.match(/,/g);
        var num = (commas && commas.length) || 0;
        ;
        return num;
    }
    function showSvara(text) {
        if (text === '_') {
            return "";
        }
        switch (text.length) {
            case 3:
                if (text[1] === '+' && text[2] === '+') {
                    return text[0] + kSvaraOctaveSuffix.overddot;
                } else if (text[1] === '-' && text[2] === '-') {
                    return text[0] + kSvaraOctaveSuffix.underddot;
                } else {
                    return text;
                }
            case 2:
                if (text[1] === '+') {
                    return text[0] + kSvaraOctaveSuffix.overdot;
                } else if (text[1] === '-') {
                    return text[0] + kSvaraOctaveSuffix.underdot;
                } else {
                    return text;
                }
            default:
                return text;
        }
    }
    function showSyllable(text) {
        if (text === '_') {
            return "";
        } else {
            return text;
        }
    }
    function svgelem(elem, n, attrs, content) {
        var tag = window.document.createElementNS('http://www.w3.org/2000/svg', n);
        if (attrs) {
            Object.keys(attrs).forEach(function (k) {
                if (attrs[k]) {
                    tag.setAttribute(k, attrs[k]);
                }
            });
        }
        if (content) {
            tag.textContent = content;
        }
        elem.appendChild(tag);
        return tag;
    }
    function loadStyleDefaults(style) {
        style[keyLineSpacing] = (+style[keyLineSpacing]) || 22;
        style[keyParaSpacing] = (+style[keyParaSpacing]) || style[keyLineSpacing];
        style[keyNotationFontSize] = (+style[keyNotationFontSize]) || 13;
        style[keyNotationSmallFontSize] = (+style[keyNotationSmallFontSize]) || (style[keyNotationFontSize] - 2);
        style[keyNotationFont] = (style[keyNotationFont] || 'sans-serif');
        style[keyTextFont] = (style[keyTextFont] || 'serif');
        style[keyStretch] = (+style[keyStretch]) || 1.0;
        style[keyStretchSpace] = (+style[keyStretchSpace]) || 1.0;
        style[keyMarginTop] = (+style[keyMarginTop]) || style[keyLineSpacing];
        style[keyMarginLeft] = (+style[keyMarginLeft]) || 10;
        style[keyLineExtensionTop] = (+style[keyLineExtensionTop]) || 0;
        style[keyLineExtensionBottom] = (+style[keyLineExtensionBottom]) || 5;
    }
    return div;
}
    Carnot['renderNotation'] = function (notation, style) {
        style = style || {};
        var pre = GLOBAL.document.createElement('pre');
        pre.textContent = notation;
        return RenderSVG(GLOBAL, Parse(pre), style);
    };
    Carnot['renderSections'] = function (sectionSelector, style) {
	    console.log('in rendersections');
        style = style || {};
        var sections = (typeof(sectionSelector) === 'string'
            ? GLOBAL.document.querySelectorAll(sectionSelector)
            : sectionSelector);
        ;
        var svgs = [], i, N;
        sections = Array.prototype.slice.call(sections);
        sections.forEach(function (s) { return s.hidden = true; });
        for (i = 0, N = sections.length; i < N; ++i) {
            svgs.push(RenderSVG(GLOBAL, Parse(sections[i]), style));
            sections[i].parentElement.insertBefore(svgs[i], sections[i]);
            Carnot.emit('rendered_section', svgs[i]);
        }
        Carnot.emit('render_complete', svgs);
    };
    Carnot['renderPage'] = function (style) {
	    console.log('in renderpage');
        /*if (GLOBAL.document.readyState === 'interactive') {
		console.log('interactive');
            setTimeout(Carnot.renderSections, 0, findSections(GLOBAL.document), style);
        } else {
            GLOBAL.document.addEventListener('readystatechange', function () {
                if (GLOBAL.document.readyState === 'interactive') {
                    Carnot.renderSections('pre.carnot_section', style);
                    GLOBAL.document.removeEventListener('readystatechange', arguments.callee);
                }
            });
        }*/
            Carnot.renderSections(findSections(GLOBAL.document), style);
            // Carnot.renderSections('pre.carnot_section', style);
    };
    Carnot['findSections'] = findSections;
    Carnot['scanStyle'] = scanStyle;
    function findSections(topNode) {
	    console.log('in findsections');
        topNode = topNode || GLOBAL.document;
        var explicitSections = topNode.querySelectorAll('pre.carnot_section');
        var sections = Array.prototype.slice.call(explicitSections);
        var allPreTags = topNode.querySelectorAll('pre');
        var i, N, pre;
        for (i = 0, N = allPreTags.length; i < N; ++i) {
            pre = allPreTags[i];
            if (!pre.classList.contains('carnot_ignore')) {
                if (/^\s*tala\s+pattern\s*=/.test(pre.textContent)) {
                    sections.push(pre);
                }
            }
        }
        return sections;
    }
    function scanStyle(topNode) {
        topNode = topNode || GLOBAL.document;
        var style = topNode.querySelector('pre.carnot_style');
        if (style) {
            style.hidden = true;
            style = Parse(style);
            return style[style.length - 1].properties;
        } else {
            return {};
        }
    }
    function renderDocWithStyle() {
        var style = GLOBAL.document.querySelector('pre.carnot_style');
        if (style) {
            style.hidden = true;
            style = Parse(style)[0].properties;
        } else {
            style = {};
        }
	console.log('going to renderpage');
        Carnot.renderPage(style);
    }
    if (GLOBAL.document.readyState === "interactive") {
        renderDocWithStyle();
    } else {
        GLOBAL.document.addEventListener('readystatechange', function () {
            if (GLOBAL.document.readyState === 'interactive') {
                renderDocWithStyle();
                GLOBAL.document.removeEventListener('readystatechange', arguments.callee);
            }
        });
    }
    window.Carnot=Carnot;
    window.renderDocWithStyle=renderDocWithStyle;
    return Carnot;
}({}));
