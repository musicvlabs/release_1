var nodes;
var edges;
var score=0;
var total=0;
var pos;
var ans;
var dic={"raga":[],"thaat":[],"jati":[],"time":[],"svar":[],"pakad":[],"aaroh":[],"avroh":[]};
var raga_objects={};
var svar_map={'S':0,'r':1,'R':2,'g':3,'G':4,'m':5,'M':6,'P':7,'d':8,'D':9,'n':10,'N':11};
var svars=['S','r','R','g','G','m','M','P','d','D','n','N'];
var default_dictionary={'S':[],'r':[],'R':[],'g':[],'G':[],'m':[],'M':[],'P':[],'d':[],'D':[],'n':[],'N':[]};
var moorchanas=[];
String.prototype.replaceAt=function(index, character) 
{
	return this.substr(0, index) + character + this.substr(index+1,12);
}
$(document).ready(function()
		{
		$.ajax(
			{
type:"GET",
url:"/static/data/nodesold.csv",
dataType: "text",
success: function(data) { parsenodes(data); }
});
		$.ajax(
			{
type:"GET",
url: "/static/data/edgesold.csv",
dataType: "text",
success: function(data) { parseedges(data); }
});
		});
function parsenodes(data)
{
	console.log("opening file");
	nodes=$.csv.toObjects(data);
	console.log("file parsed");
}
function parseedges(data)
{
	console.log("opening file");
	edges=$.csv.toObjects(data);
	console.log("file parsed");
	distribute();
}
function distribute()
{
	for(i=1;i<386;i++)
	{
		x=nodes[i-1]["Type"].replace(/'/g, "");
		dic[x].push(nodes[i-1].Label.replace(/'/g,""));
	}
	dropdown_ragas=document.getElementById("ragas");
	dropdown_category=document.getElementById("category");
	for(i=0;i<163;i++)
	{
		var option=document.createElement('option');
		option.text=option.value=dic['raga'][i];
		dropdown_ragas.add(option,0);
		raga_objects[dic['raga'][i]]={binary_aaroh:'000000000000',binary_avroh:'000000000000',binary_both:'000000000000',moorchanas_aaroh:{'S':[],'r':[],'R':[],'g':[],'G':[],'m':[],'M':[],'P':[],'d':[],'D':[],'n':[],'N':[]},moorchanas_avroh:{'S':[],'r':[],'R':[],'g':[],'G':[],'m':[],'M':[],'P':[],'d':[],'D':[],'n':[],'N':[]},moorchanas_both:{'S':[],'r':[],'R':[],'g':[],'G':[],'m':[],'M':[],'P':[],'d':[],'D':[],'n':[],'N':[]}};
	}
	for(i=979;i<3750;i++)
	{
		var svar_name=nodes[edges[i-1].Target-1].Label.replace(/'/g, "");
		var raga_name=dic['raga'][edges[i-1].Source-1].replace(/'/g,"");
		if(svar_name=='')
			continue;
		raga_objects[raga_name].binary_aaroh=raga_objects[raga_name].binary_aaroh.replaceAt(svar_map[svar_name],'1');
		raga_objects[raga_name].binary_both=raga_objects[raga_name].binary_both.replaceAt(svar_map[svar_name],'1');
	}
	for(i=3750;i<7004;i++)
	{
		var svar_name=nodes[edges[i-1].Target-1].Label.replace(/'/g, "");
		var raga_name=dic['raga'][edges[i-1].Source-1].replace(/'/g,"");
		if(svar_name=='')
			continue;
		raga_objects[raga_name].binary_avroh=raga_objects[raga_name].binary_avroh.replaceAt(svar_map[svar_name],'1');
		raga_objects[raga_name].binary_both=raga_objects[raga_name].binary_both.replaceAt(svar_map[svar_name],'1');
	}
	console.log(raga_objects);
	generate();
}
function generate()
{
	list_of_ragas=Object.keys(raga_objects);
	for(i=0;i<list_of_ragas.length;i++)
	{
		var shifted_string_aaroh=raga_objects[list_of_ragas[i]].binary_aaroh;
		var shifted_string_avroh=raga_objects[list_of_ragas[i]].binary_avroh;
		var shifted_string_both=raga_objects[list_of_ragas[i]].binary_both;
		var count=0;
		while(count!=12)
		{
			for(j=0;j<list_of_ragas.length;j++)
			{
				if(j==i)
					continue;
				if(shifted_string_aaroh[0]=='1' && shifted_string_aaroh.localeCompare(raga_objects[list_of_ragas[j]].binary_aaroh)==0)
				{
					raga_objects[list_of_ragas[i]].moorchanas_aaroh[svars[count]].push(list_of_ragas[j]);
				}
				if(shifted_string_avroh[0]=='1' && shifted_string_avroh.localeCompare(raga_objects[list_of_ragas[j]].binary_avroh)==0)
				{
					raga_objects[list_of_ragas[i]].moorchanas_avroh[svars[count]].push(list_of_ragas[j]);
				}
				if(shifted_string_both[0]=='1' && shifted_string_both.localeCompare(raga_objects[list_of_ragas[j]].binary_both)==0)
				{
					raga_objects[list_of_ragas[i]].moorchanas_both[svars[count]].push(list_of_ragas[j]);
				}
			}	
			shifted_string_aaroh=shifted_string_aaroh.substr(1,12)+shifted_string_aaroh[0];
			shifted_string_avroh=shifted_string_avroh.substr(1,12)+shifted_string_avroh[0];
			shifted_string_both=shifted_string_both.substr(1,12)+shifted_string_both[0];
			count++;
		}
	}
}

var count=0;
var list_of_ragas=Object.keys(raga_objects);
var table=document.getElementById("table");

function submit(raga_id,category)
{
	var ragas=document.getElementById("ragas");
	var raga_name;
	if(raga_id==undefined)
	{
		for(i=0;i<count;i++)
			table.deleteRow(0);
		raga_name=ragas.options[ragas.selectedIndex].text;
		count=0;
	}
	else
		raga_name=list_of_ragas[raga_id];
	var categories=document.getElementById("category");
	if(category==undefined)
	{
		for(i=0;i<count;i++)
			table.deleteRow(0);
		count=0;
		category=categories.options[categories.selectedIndex].text;
	}
	console.log('raga id: '+raga_id);
	console.log('category: '+category);
	console.log('count:'+count);
	console.log('rows: '+table.rows.length);
	var row;
	var shifted_string;
	var sequence;
	var i;
	row=table.insertRow(count);
	count++;
	row=table.insertRow(count);
	count++;
	row=table.insertRow(count);
	var col=row.insertCell(0);
	col.innerHTML=raga_name;
	count++;
	row=table.insertRow(count);
	count++;
	col=row.insertCell(0);
	col.innerHTML='Svar';
	col=row.insertCell(1);
	col.innerHTML='Sequence';
	col=row.insertCell(2);
	col.innerHTML='Ragas';
	if(raga_name=='undefined')
	{
		console.log('undefined');
		return;
	}
	if(category=='Aaroh')
	{
		instance=raga_objects[raga_name].moorchanas_aaroh;
		for(i=0;i<12;i++)
		{
			if(instance[svars[i]].length==0)
				continue;
			shifted_string=raga_objects[raga_name].binary_aaroh.substr(i,12)+raga_objects[raga_name].binary_aaroh.substr(0,i);
			sequence=convert_to_svar(shifted_string);
			row=table.insertRow(count);
			count++;
			var col1=row.insertCell(0);
			var col2=row.insertCell(1);
			var col3=row.insertCell(2);
			col1.innerHTML=svars[i];
			col2.innerHTML=sequence;
			moorchana_links=[];
			//console.log('in submit');
			for(j=0;j<instance[svars[i]].length;j++)
			{
				col3.innerHTML=col3.innerHTML+'<a href="#" onclick="submit('+list_of_ragas.indexOf(instance[svars[i]][j])+','+"'Aaroh'"+');"'+'>'+instance[svars[i]][j]+'</a>'+'   ';
			}
			//col3.innerHTML=moorchana_links;	//Change this to have links for each raga
		}
	}
	else if(category=='Avroh')
	{
		instance=raga_objects[raga_name].moorchanas_avroh;
		for(i=11;i>=0;i--)
		{
			if(instance[svars[i]].length==0)
				continue;
			shifted_string=raga_objects[raga_name].binary_avroh.substr(i,12)+raga_objects[raga_name].binary_avroh.substr(0,i);
			sequence=convert_to_svar(shifted_string);
			sequence=sequence.split("").reverse().join("");
			row=table.insertRow(count);
			count++;
			var col1=row.insertCell(0);
			var col2=row.insertCell(1);
			var col3=row.insertCell(2);
			col1.innerHTML=svars[i];
			col2.innerHTML=sequence;
			for(j=0;j<instance[svars[i]].length;j++)
			{
				col3.innerHTML=col3.innerHTML+'<a href="#" onclick="submit('+list_of_ragas.indexOf(instance[svars[i]][j])+','+"'Avroh'"+');"'+'>'+instance[svars[i]][j]+'</a>'+'   ';
			}
			//col3.innerHTML=instance[svars[i]];	//Change this to have links for each raga
		}
	}
	else
	{
		instance=raga_objects[raga_name].moorchanas_both;
		for(i=0;i<12;i++)
		{
			if(instance[svars[i]].length==0)
				continue;
			shifted_string=raga_objects[raga_name].binary_both.substr(i,12)+raga_objects[raga_name].binary_both.substr(0,i);
			sequence=convert_to_svar(shifted_string);
			row=table.insertRow(count);
			count++;
			var col1=row.insertCell(0);
			var col2=row.insertCell(1);
			var col3=row.insertCell(2);
			col1.innerHTML=svars[i];
			col2.innerHTML=sequence;
			for(j=0;j<instance[svars[i]].length;j++)
			{
				col3.innerHTML=col3.innerHTML+'<a href="#" onclick="submit('+list_of_ragas.indexOf(instance[svars[i]][j])+','+"'Both'"+');"'+'>'+instance[svars[i]][j]+'</a>'+'   ';
			}
			//console.log(col3.innerHTML);
			//col3.innerHTML=instance[svars[i]];	//Change this to have links for each raga
		}

	}
}
function convert_to_svar(binary)
{
	var sequence='';
	var i;
	for(i=0;i<binary.length;i++)
		if(binary[i]=='1')
			sequence=sequence+svars[i]+' ';
	return sequence;
}
