(function(window) {
var path = '/static/bols';
var figpath = '/static/figures';
var bol_elem = document.createElement('audio');
var bol_dha = ["dhe","dha1"];
var bol_dhir = ["dhir1"];
var bol_dhin = ["dhin1","dhun1"];
var bol_ge  = ["ge1","ge2","ge3","ge4","ge5","ge6","ge7","ge8","ge9","ghe1"];
var bol_ka  = ["ka1","ka2","ka3","kat1"];
var bol_na  = ["na1","na2","na3","na4","na5"];
var bol_re  = ["re1","re2"];
var bol_ta  = ["ta1","ta3","ta4","ta5","te1"];
var bol_taka = ["taka1"];
var bol_tita = ["tita1", "tita2"];
var bol_tun = ["tun1", "tun2", "tun3", "tun4"];
var bol_tuh = ["tuh1", "tuh2", "tuh3"];
var bol_tin = ["tin1", "tin2", "tin3"];



var canvas = document.getElementById('figures');
var context = canvas.getContext('2d');




function playbol_dha() {
context.clearRect(0, 0, canvas.width, canvas.height);
var note =  bol_dha[Math.floor(Math.random() * bol_dha.length)];
var currentnote_path = path + '/'  + note + '.wav';
bol_elem.setAttribute('src', currentnote_path);
  bol_elem.play();
  context.clearRect(0, 0, canvas.width, canvas.height);
  image = new Image();
  image.src = '/static/figures/dha.jpg';
  context.drawImage(image,  0, 0, image.width,    image.height,
                       0, 0, canvas.width, canvas.height);
}

function playbol_dhir() {
context.clearRect(0, 0, canvas.width, canvas.height);
var note =  bol_dha[Math.floor(Math.random() * bol_dhir.length)];
var currentnote_path = path + '/'  + note + '.wav';
bol_elem.setAttribute('src', currentnote_path);
  bol_elem.play();
  context.clearRect(0, 0, canvas.width, canvas.height);
  image = new Image();
  image.src = '/static/figures/dhir.jpg';
  context.drawImage(image,  0, 0, image.width,    image.height,
                       0, 0, canvas.width, canvas.height);
}

function playbol_ge() {
var note =  bol_ge[Math.floor(Math.random() * bol_ge.length)];
var currentnote_path = path + '/'  + note + '.wav';
bol_elem.setAttribute('src', currentnote_path);
  bol_elem.play();
  context.clearRect(0, 0, canvas.width, canvas.height);
  image = new Image();
  image.src = '/static/figures/ghe.jpg';
  context.drawImage(image,  0, 0, image.width,    image.height,
                       0, 0, canvas.width, canvas.height);
}

function playbol_dhin() {
var note =  bol_ge[Math.floor(Math.random() * bol_dhin.length)];
var currentnote_path = path + '/'  + note + '.wav';
bol_elem.setAttribute('src', currentnote_path);
  bol_elem.play();
  context.clearRect(0, 0, canvas.width, canvas.height);
  image = new Image();
  image.src = '/static/figures/dhin.jpg';
  context.drawImage(image,  0, 0, image.width,    image.height,
                       0, 0, canvas.width, canvas.height);
}

function playbol_ka() {
var note =  bol_ka[Math.floor(Math.random() * bol_ka.length)];
var currentnote_path = path + '/'  + note + '.wav';
bol_elem.setAttribute('src', currentnote_path);
  bol_elem.play();
  image = new Image();
  image.src = '/static/figures/ka.jpg';
  context.drawImage(image,  0, 0, image.width,    image.height,
                       0, 0, image.width, image.height);
  }
function playbol_na() {
var note =  bol_na[Math.floor(Math.random() * bol_na.length)];
var currentnote_path = path + '/'  + note + '.wav';
bol_elem.setAttribute('src', currentnote_path);
  bol_elem.play();
  image = new Image();
  image.src = '/static/figures/na.jpg';
  context.drawImage(image,  0, 0, image.width,    image.height,
                       0, 0, image.width, image.height);
  }

  function playbol_tin() {
  context.clearRect(0, 0, canvas.width, canvas.height);
  var note =  bol_dha[Math.floor(Math.random() * bol_tin.length)];
  var currentnote_path = path + '/'  + note + '.wav';
  bol_elem.setAttribute('src', currentnote_path);
    bol_elem.play();
    context.clearRect(0, 0, canvas.width, canvas.height);
    image = new Image();
    image.src = '/static/figures/tin.jpg';
    context.drawImage(image,  0, 0, image.width,    image.height,
                         0, 0, canvas.width, canvas.height);
  }

function playbol_re() {
var note =  bol_re[Math.floor(Math.random() * bol_re.length)];
var currentnote_path = path + '/'  + note + '.wav';
bol_elem.setAttribute('src', currentnote_path);
  bol_elem.play();
  image = new Image();
  image.src = '/static/figures/re.jpg';
  context.drawImage(image,  0, 0, image.width,    image.height,
                       0, 0, canvas.width, canvas.height);
  }

function playbol_ta() {
var note =  bol_ta[Math.floor(Math.random() * bol_ta.length)];
var currentnote_path = path + '/'  + note + '.wav';
bol_elem.setAttribute('src', currentnote_path);
  bol_elem.play();
  image = new Image();
  image.src = '/static/figures/ta.jpg';
  context.drawImage(image,  0, 0, image.width,    image.height,
                       0, 0, canvas.width, canvas.height);
  }


function playbol_taka() {
var note =  bol_dha[Math.floor(Math.random() * bol_taka.length)];
var currentnote_path = path + '/'  + note + '.wav';
bol_elem.setAttribute('src', currentnote_path);
  bol_elem.play();
  image = new Image();
  image.src = '/static/figures/taka.jpg';
  context.drawImage(image,  0, 0, image.width,    image.height,
                       0, 0, canvas.width, canvas.height);
}


function playbol_tita() {
var note =  bol_dha[Math.floor(Math.random() * bol_tita.length)];
var currentnote_path = path + '/'  + note + '.wav';
bol_elem.setAttribute('src', currentnote_path);
  bol_elem.play();
  image = new Image();
  image.src = '/static/figures/tita.jpg';
  context.drawImage(image,  0, 0, image.width,    image.height,
                       0, 0, canvas.width, canvas.height);
}


function playbol_tun() {
var note =  bol_dha[Math.floor(Math.random() * bol_tun.length)];
var currentnote_path = path + '/'  + note + '.wav';
bol_elem.setAttribute('src', currentnote_path);
  bol_elem.play();
  image = new Image();
  image.src = '/static/figures/tun.jpg';
  context.drawImage(image,  0, 0, image.width,    image.height,
                       0, 0, canvas.width, canvas.height);
  }
function playbol_tuh() {
var note =  bol_dha[Math.floor(Math.random() * bol_tuh.length)];
var currentnote_path = path + '/'  + note + '.wav';
bol_elem.setAttribute('src', currentnote_path);
  bol_elem.play();
  image = new Image();
  image.src = '/static/figures/tuh.jpg';
  context.drawImage(image,  0, 0, image.width,    image.height,
                       0, 0, canvas.width, canvas.height);
  }

window.playbol_dha = playbol_dha;
window.playbol_ge = playbol_ge;
window.playbol_dhin = playbol_dhin;
window.playbol_ka = playbol_ka;
window.playbol_na = playbol_na;
window.playbol_re = playbol_re;
window.playbol_ta = playbol_ta;
window.playbol_taka = playbol_taka;
window.playbol_tita = playbol_tita;
window.playbol_tun = playbol_tun;
window.playbol_tuh = playbol_tuh;
window.playbol_tin = playbol_tin;
window.playbol_dhir = playbol_dhir;

})(window);
