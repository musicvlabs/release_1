(function(window) {

// global vars..
var current_note,
    tone_elem,
    drone_elem,
    path = '/static/stimuli/instruments',
    note_array = ["c3","cs3","d3","ds3","e3","f3","fs3","g3","gs3","a3","as3","b3","c4"],
    note_names = ["Sa","re","Re","ga","Ga","Ma","Ma#","Pa","dha","Dha","ni","Ni","Sa1"],
    levels = {
      "1": [0, 5, 7],
      "2": [0, 5, 7, 4, 11],
      "3": [0, 5, 7, 2, 9],
      "4": [0, 5, 7, 4, 11, 2, 9],
      "5": [0, 5, 7, 3, 10],
      "6": [0, 5, 7, 1, 8],
      "7": [0, 5, 7, 6],
      "8": [0, 5, 7, 4, 11, 2, 9, 3, 10, 1, 8, 6]
    },
    playing = false,
    correct = 0,
    incorrect = 0;


// initialize stuff..
function init() {
  // append audio elements
  tone_elem = document.createElement('audio');
  drone_elem = document.createElement('audio');
  drone_elem.setAttribute('src', '/static/drones/3.mp3');

  console.log('initing');

  // attach level selector handlers
  // highlight the current level selector
  //$("#level-selector").delegate("li", "click", function() {
  $("#level-selector li a").click(function() {
    $('#level-selector li').removeClass("active");
    $(this).parent().addClass("active");
  });

  // attach flip panel click handlers
  // shows corresponding panel when clicked on level selector
  $(".flip").click(function(e) {
    e.preventDefault();
    var target = $(this).attr("href");
    $(target).slideToggle("fast");
    $(".panel").not(target).hide();
    return false;
  });

}

// handler when play tone btn clicked
function playtone_clicked() {
   var instrument= $('#instrument option:selected').val();
   var level = $('#level-selector li.active').val();
   console.log(level, instrument);
   play_tone(level, instrument);
}

// generic function to play a tone based on level and instrument
function play_tone(level, instrument) {
  var currentnote_path;
  // if user has not submitted, play the current note
  if(playing === true) {
    currentnote_path = path + '/' + instrument + '/' +
      current_note + '.mp3';
  }
  // else generate a new random note; and that becomes the current note
  else {
    current_note = get_random_note(level);
    currentnote_path = path + '/' + instrument + '/' +
      current_note + '.mp3';
    playing = true;
  }

  tone_elem.setAttribute('src', currentnote_path);
  tone_elem.play();
}

// return a random note based on the level given..
function get_random_note(level) {
  console.log('got level', level);
  var level_array = levels[level];
  console.log('level array', level_array);
  var noteno = level_array[Math.floor(Math.random() * level_array.length)];
  console.log('note no', noteno);
  return note_array[noteno];
}

function submit_clicked() {
  var level = $('#level-selector li.active').val();
  var user_ip = $('#l' + level + ' input:checked').val();
  console.log(user_ip);
  check_submit(user_ip);
}

function check_submit(user_ip) {
  // if user is not currently playing(meaning previously submitted the same
  // input); simply return; don't evaluate..
  if(playing == false) {
    return;
  }

  var i = note_names.indexOf(user_ip);

  if(note_array[i] == current_note) {
    $('#answer').html('Right');
    //var right = document.getElementById("div1-1").innerHTML = ('Right');
    correct = correct + 1;
    // set playing to false only if the user has got it right..
    // i.e no retrying next time..
    playing = false;
  }
  else {
    $('#answer').html('Wrong');
    //var wrong = document.getElementById("div1-1").innerHTML = ('Wrong');
    incorrect = incorrect + 1;
  }

  var score = correct + incorrect;
  $('#score').html('Your score is ' + correct + ' out of ' + score);
  //document.getElementById("div1-2").innerHTML=("Your score is" + '&nbsp;' + window.correct + '&nbsp;' + "out of" + '&nbsp;' + r);
}

function play_tonic() {
  drone_elem.play();
}

function stop_tonic() {
  drone_elem.pause();
}

// expose public methods..
window.init = init;
window.playtone_clicked = playtone_clicked;
window.submit_clicked = submit_clicked;
window.play_tonic = play_tonic;
window.stop_tonic = stop_tonic;

})(window);
