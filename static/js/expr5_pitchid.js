(function(window) {

// global vars..
var current_note,
    tone_elem,
    drone_elem,
    path = '/static/stimuli/instruments',
    note_array = ["c3","cs3","d3","ds3","e3","f3","fs3","g3","gs3","a3","as3","b3","c4"],
    note_names = ["Sa","re","Re","ga","Ga","Ma","Ma#","Pa","dha","Dha","ni","Ni","Sa1"],
    levels = {
      "1": [0, 5, 7],
      "2": [0, 5, 7, 4, 11],
      "3": [0, 5, 7, 2, 9],
      "4": [0, 5, 7, 4, 11, 2, 9],
      "5": [0, 5, 7, 3, 10],
      "6": [0, 5, 7, 1, 8],
      "7": [0, 5, 7, 6],
      "8": [0, 5, 7, 4, 11, 2, 9, 3, 10, 1, 8, 6]
    },
    playing = false,
    tonicplaying=false,
    correct = 0,
    incorrect = 0;

// initialize stuff..
function init() {
  // append audio elements
  tone_elem = document.createElement('audio');
  drone_elem = document.createElement('audio');
  drone_elem.setAttribute('src', '/static/drones/3.mp3');
  console.log('initing');

  // attach level selector handlers
  // highlight the current level selector
  //$("#level-selector").delegate("li", "click", function() {
  $("#level-selector li a").click(function() {
    $('#level-selector li').removeClass("active");
    $(this).parent().addClass("active");
  });

  // attach flip panel click handlers
  // shows corresponding panel when clicked on level selector
  $(".flip").click(function(e) {
    e.preventDefault();
    var target = $(this).attr("href");
    $(target).slideToggle("fast");
    $(".panel").not(target).hide();
    var level=$('#level-selector li.active').val();
    console.log(level);
    if(level==1)
    {
    	if(tonicplaying==true)
	{
		tonicplaying=false;
		stop_tonic();
	}
    	Mousetrap.reset();
	var swars=document.getElementsByName("swar1");
	swars[0].checked=false;
	swars[1].checked=false;
	swars[2].checked=false;
    	Mousetrap.bind(	'1', function() { swars[0].checked=true; });
	Mousetrap.bind(	'5', function() { swars[1].checked=true; });
	Mousetrap.bind(	'4', function() { swars[2].checked=true; });
    	Mousetrap.bind('shift+space', function(e) { e.preventDefault(); if(tonicplaying==true) { stop_tonic(); } else { play_tonic(); } });
	Mousetrap.bind('space', function(e) { e.preventDefault(); playtone_clicked(); });
	Mousetrap.bind('enter', function(e) { e.preventDefault(); submit_clicked(); });
    }
    else if(level==2)
    {
	if(tonicplaying==true)
	{
		tonicplaying=false;
		stop_tonic();
	}

	Mousetrap.reset();
	var swars=document.getElementsByName("swar2");
	swars[0].checked=false;
	swars[1].checked=false;
	swars[2].checked=false;
	swars[3].checked=false;
	swars[4].checked=false;
    	Mousetrap.bind('1', function() { swars[0].checked=true; });
    	Mousetrap.bind('5', function() { swars[1].checked=true; });
    	Mousetrap.bind('4', function() { swars[2].checked=true; });
    	Mousetrap.bind('3', function() { swars[3].checked=true; });
    	Mousetrap.bind('7', function() { swars[4].checked=true; });
    	Mousetrap.bind('shift+space', function(e) { e.preventDefault(); if(tonicplaying==true) { stop_tonic(); } else { play_tonic(); } });
	Mousetrap.bind('space', function(e) { e.preventDefault(); playtone_clicked(); });
	Mousetrap.bind('enter', function(e) { e.preventDefault(); submit_clicked(); });
    }
    else if(level==3)
    {
    	if(tonicplaying==true)
	{
		tonicplaying=false;
		stop_tonic();
	}
	Mousetrap.reset();
	var swars=document.getElementsByName("swar3");
	swars[0].checked=false;
	swars[1].checked=false;
	swars[2].checked=false;
	swars[3].checked=false;
	swars[4].checked=false;
    	Mousetrap.bind('1', function() { swars[0].checked=true;  });
    	Mousetrap.bind('5', function() { swars[1].checked=true;  });
    	Mousetrap.bind('4', function() { swars[2].checked=true;  });
    	Mousetrap.bind('2', function() { swars[3].checked=true;  });
    	Mousetrap.bind('6', function() { swars[4].checked=true;  });
    	Mousetrap.bind('shift+space', function(e) { e.preventDefault(); if(tonicplaying==true) { stop_tonic(); } else { play_tonic(); } });
	Mousetrap.bind('space', function(e) { e.preventDefault(); playtone_clicked(); });
	Mousetrap.bind('enter', function(e) { e.preventDefault(); submit_clicked(); });
    }
    else if(level==4)
    {
	if(tonicplaying==true)
	{
		tonicplaying=false;
		stop_tonic();
	}
	Mousetrap.reset();
	var swars=document.getElementsByName("swar4");
	swars[0].checked=false;
	swars[1].checked=false;
	swars[2].checked=false;
	swars[3].checked=false;
	swars[4].checked=false;
	swars[5].checked=false;
	swars[6].checked=false;
    	Mousetrap.bind('1', function() { swars[0].checked=true; });
    	Mousetrap.bind('5', function() { swars[1].checked=true; });
    	Mousetrap.bind('4', function() { swars[2].checked=true; });
    	Mousetrap.bind('3', function() { swars[3].checked=true; });
    	Mousetrap.bind('7', function() { swars[4].checked=true; });
    	Mousetrap.bind('2', function() { swars[5].checked=true; });
    	Mousetrap.bind('6', function() { swars[6].checked=true; });
    	Mousetrap.bind('shift+space', function(e) { e.preventDefault(); if(tonicplaying==true) { stop_tonic(); } else { play_tonic(); } });
	Mousetrap.bind('space', function(e) { e.preventDefault(); playtone_clicked(); });
	Mousetrap.bind('enter', function(e) { e.preventDefault(); submit_clicked(); });
    }
    else if(level==5)
    {
    	if(tonicplaying==true)
	{
		tonicplaying=false;
		stop_tonic();
	}
	Mousetrap.reset();
	var swars=document.getElementsByName("swar5");
	swars[0].checked=false;
	swars[1].checked=false;
	swars[2].checked=false;
	swars[3].checked=false;
	swars[4].checked=false;
    	Mousetrap.bind('1', function() { swars[0].checked=true; });
    	Mousetrap.bind('5', function() { swars[1].checked=true; });
    	Mousetrap.bind('4', function() { swars[2].checked=true; });
    	Mousetrap.bind('shift+3', function() { swars[3].checked=true; });
    	Mousetrap.bind('shift+7', function() { swars[4].checked=true; });
    	Mousetrap.bind('shift+space', function(e) { e.preventDefault(); if(tonicplaying==true) { stop_tonic(); } else { play_tonic(); } });
	Mousetrap.bind('space', function(e) { e.preventDefault(); playtone_clicked(); });
	Mousetrap.bind('enter', function(e) { e.preventDefault(); submit_clicked(); });
    }
    else if(level==6)
    {
    	if(tonicplaying==true)
	{
		tonicplaying=false;
		stop_tonic();
	}
	Mousetrap.reset();
	var swars=document.getElementsByName("swar6");
	swars[0].checked=false;
	swars[1].checked=false;
	swars[2].checked=false;
	swars[3].checked=false;
	swars[4].checked=false;
    	Mousetrap.bind('1', function() { swars[0].checked=true; });
    	Mousetrap.bind('5', function() { swars[1].checked=true; });
    	Mousetrap.bind('4', function() { swars[2].checked=true; });
    	Mousetrap.bind('shift+2', function() { swars[3].checked=true; });
    	Mousetrap.bind('shift+6', function() { swars[4].checked=true; });
    	Mousetrap.bind('shift+space', function(e) { e.preventDefault(); if(tonicplaying==true) { stop_tonic(); } else { play_tonic(); } });
	Mousetrap.bind('space', function(e) { e.preventDefault(); playtone_clicked(); });
	Mousetrap.bind('enter', function(e) { e.preventDefault(); submit_clicked(); });
    }
    else if(level==7)
    {
    	if(tonicplaying==true)
	{
		tonicplaying=false;
		stop_tonic();
	}
	Mousetrap.reset();
	var swars=document.getElementsByName("swar7");
	swars[0].checked=false;
	swars[1].checked=false;
	swars[2].checked=false;
	swars[3].checked=false;
    	Mousetrap.bind('1', function() { swars[0].checked=true; });
    	Mousetrap.bind('5', function() { swars[1].checked=true; });
    	Mousetrap.bind('4', function() { swars[2].checked=true; });
    	Mousetrap.bind('shift+4', function() { swars[3].checked=true; });
    	Mousetrap.bind('shift+space', function(e) { e.preventDefault(); if(tonicplaying==true) { stop_tonic(); } else { play_tonic(); } });
	Mousetrap.bind('space', function(e) { e.preventDefault(); playtone_clicked(); });
	Mousetrap.bind('enter', function(e) { e.preventDefault(); submit_clicked(); });
    }
    else if(level==8)
    {
    	if(tonicplaying==true)
	{
		tonicplaying=false;
		stop_tonic();
	}
	Mousetrap.reset();
	var swars=document.getElementsByName("swar8");
	swars[0].checked=false;
	swars[1].checked=false;
	swars[2].checked=false;
	swars[3].checked=false;
	swars[4].checked=false;
	swars[5].checked=false;
	swars[6].checked=false;
	swars[7].checked=false;
	swars[8].checked=false;
	swars[8].checked=false;
	swars[10].checked=false;
	swars[11].checked=false;
    	Mousetrap.bind('1', function() { swars[0].checked=true; });
    	Mousetrap.bind('5', function() { swars[1].checked=true; });
    	Mousetrap.bind('4', function() { swars[2].checked=true; });
    	Mousetrap.bind('3', function() { swars[3].checked=true; });
    	Mousetrap.bind('7', function() { swars[4].checked=true; });
    	Mousetrap.bind('2', function() { swars[5].checked=true; });
    	Mousetrap.bind('6', function() { swars[6].checked=true; });
    	Mousetrap.bind('shift+3', function() { swars[7].checked=true; });
    	Mousetrap.bind('shift+7', function() { swars[8].checked=true; });
    	Mousetrap.bind('shift+2', function() { swars[9].checked=true; });
    	Mousetrap.bind('shift+6', function() { swars[10].checked=true; });
    	Mousetrap.bind('shift+4', function() { swars[11].checked=true; });
    	Mousetrap.bind('shift+space', function(e) { e.preventDefault(); if(tonicplaying==true) { stop_tonic(); } else { play_tonic(); } });
	Mousetrap.bind('space', function(e) { e.preventDefault(); playtone_clicked(); });
	Mousetrap.bind('enter', function(e) { e.preventDefault(); submit_clicked(); });
    }
    return false;
  });

  ML.collectRetentionAndSave(5);
}

// handler when play tone btn clicked
function playtone_clicked() {
   var instrument= $('#instrument option:selected').val();
   var level = $('#level-selector li.active').val();
   console.log(level, instrument);
   play_tone(level, instrument);
}

// generic function to play a tone based on level and instrument
function play_tone(level, instrument) {
  var currentnote_path;
  // if user has not submitted, play the current note
  if(playing === true) {
    currentnote_path = path + '/' + instrument + '/' +
      current_note + '.mp3';
  }
  // else generate a new random note; and that becomes the current note
  else {
    current_note = get_random_note(level);
    currentnote_path = path + '/' + instrument + '/' +
      current_note + '.mp3';
    playing = true;
  }
    tone_elem.setAttribute('src', currentnote_path);
  tone_elem.play();
}

// return a random note based on the level given..
function get_random_note(level) {
  console.log('got level', level);
  var level_array = levels[level];
  console.log('level array', level_array);
  var noteno = level_array[Math.floor(Math.random() * level_array.length)];
  console.log('note no', noteno);
    return note_array[noteno];
}

function submit_clicked() {
  var level = $('#level-selector li.active').val();
  var user_ip = $('#l' + level + ' input:checked').val();
  console.log(level, user_ip);
  if(tonicplaying==true)
  {
	  stop_tonic();
  }
  check_submit(user_ip, level);
}

function check_submit(user_ip, level) {
  // if user is not currently playing(meaning previously submitted the same
  // input); simply return; don't evaluate..
  if(playing == false) {
    return;
  }

  var i = note_names.indexOf(user_ip);

  if(note_array[i] == current_note) {
    $('#answer').html('Right');
    correct = correct + 1;
    ML.postScore(1, 5, level);
    // set playing to false only if the user has got it right..
    // i.e no retrying next time..
    playing = false;
  }
  else {
    $('#answer').html('Wrong');
    incorrect = incorrect + 1;
    ML.postScore(0, 5, level);
  }

  var score = correct + incorrect;
  $('#score').html('Your score is ' + correct + ' out of ' + score);
  //document.getElementById("div1-2").innerHTML=("Your score is" + '&nbsp;' + window.correct + '&nbsp;' + "out of" + '&nbsp;' + r);
}

function play_tonic() {
  tonicplaying=true;
  drone_elem.play();
}

function stop_tonic() {
  tonicplaying=false;
  drone_elem.pause();
}

// expose public methods..
window.init = init;
window.playtone_clicked = playtone_clicked;
window.submit_clicked = submit_clicked;
window.play_tonic = play_tonic;
window.stop_tonic = stop_tonic;

})(window);
