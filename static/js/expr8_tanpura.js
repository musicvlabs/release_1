(function(window) {

  var freqlist = [440, 466, 493, 523, 554, 622, 659, 698, 739, 783, 830, 880];

  var isplaying = false;
  var freq=0;
  var freq_rand;
  var new_freq;
  var guess_freq = 0;

  var init = false;

  var step_size_val_map = {
    //in Hz
    "0" : 0.5, // small step
    "5" : 10,  // medium step
    "10" : 20  // large step
  };

  var tone_playing = false;

  var sineosc = function(freq) {
    // this.context = new AudioContext();
    // this.oscillator = this.context.createOscillator();
    // this.oscillator.type = 'sineosc';
    var context = new AudioContext();
    this.oscillator1 = context.createOscillator();
    this.oscillator1.type = 'sineosc';


    this.oscillator2 = context.createOscillator();
    this.oscillator2.type = 'sawtooth';

    gain1 = context.createGain();
    gain1.gain.value = 1;

    gain2 = context.createGain();
    gain2.gain.value = 1;


    Gain = context.createGain();
    this.oscillator1.connect(gain1);
    this.oscillator2.connect(gain2);
    // gain1.connect(context.destination);
    // gain2.connect(context.destination);
     gain1.connect(Gain);
    gain2.connect(Gain);
    Gain.connect(context.destination);

    this.tone_playing = false;

    return this;
  };



  sineosc.prototype.play = function(freq) {

      console.log('osc play()');
      this.oscillator1.frequency.value = freq;
      this.oscillator2.frequency.value = freq/2;
      // this.oscillator1.connect(this.context.destination);
      // this.oscillator2.connect(this.context.destination);
    this.oscillator1.start();
    this.oscillator2.start();
    this.tone_playing = true;
    init = true;
      //
      // else if(init === true){
      //   gain1.gain.value = 1;
      //   gain2.gain.value = 1;
    //this.oscillator2.connect(this.context.destination);
    //this.oscillator2.start();
    this.tone_playing = true;
    init = true;

  };




  sineosc.prototype.stop = function() {
    console.log('osc stop()');
    Gain.gain.value = 0;
    //gain.disconnect(context.destionation);
    //this.oscillator.stop();
    this.tone_playing = false;
  };

  sineosc.prototype.change = function(freq) {
    this.oscillator1.frequency.value = freq;
    this.oscillator2.frequency.value = freq/2;
  };

  var player = new sineosc();
  var guesser = new sineosc();

  var player_btn_clicked = function() {
    if($(this).hasClass('off')) {
      $(this).find('i').removeClass('fa-play').addClass('fa-pause');
      $(this).removeClass('off').addClass('on');
      play_tone();
    }
    else if($(this).hasClass('on')) {
      $(this).find('i').removeClass('fa-pause').addClass('fa-play');
      $(this).removeClass('on').addClass('off');
      player.stop();
    }
  }

  var guesser_btn_clicked = function() {
    if($(this).hasClass('off')) {
      $(this).find('i').removeClass('fa-play').addClass('fa-pause');
      $(this).removeClass('off').addClass('on');
      guess_tone();
    }
    else if($(this).hasClass('on')) {
      $(this).find('i').removeClass('fa-pause').addClass('fa-play');
      $(this).removeClass('on').addClass('off');
      guesser.stop();
    }
  }

  var on_play_click = function() {
    if(tone_playing === true){
      button1.src="ico-stop.gif";
      player.stop();
    }
    else {
      button1.src="ico-play.gif";
      play_tone();
    }
  }

  var on_guess_click = function() {
    if(tone_playing){
      guesser.stop();
      button2.src="ico-stop.gif";
    }
    else{
      guess_tone();
      button2.src="ico-play.gif";
    }
  }



  var play_tone = function() {
    if(freq) {
      tone_playing = true;
    }
    else {
      freq = freqlist[Math.floor(Math.random()*freqlist.length)];
    }
    player.play(freq);
  };

  var sineoscoff = function() {
  };

  var guess_tone = function() {
    if(guess_freq) {
      tone_playing = true;
    }
    else {
      guess_freq = Math.floor(Math.random()*((freq+50) - (freq-50)) + (freq-50));
    }
    //	console.log(guess_freq);
    guesser.play(guess_freq);
  };

  var guess_up_clicked = function() {
    //	console.log('guess up clicked')
    var step_val = $('#step-size').val();
    var step = step_size_val_map[step_val];
    guess_freq = guess_freq + step;
    guesser.change(guess_freq);
    var result = compare(guess_freq ,freq);
    print_result(result);
  }

  var guess_down_clicked = function() {
    //	console.log('guess down clicked')
    var step_val = $('#step-size').val();
    var step = step_size_val_map[step_val];
    guess_freq = guess_freq - step;
    guesser.change(guess_freq);
    var result = compare(guess_freq,freq);
    print_result(result);
  }

  var compare = function(guess_freq, freq) {
    //	console.log('compare()');
    var r = guess_freq - freq;
    //      console.log(r);
    //	console.log(guess_freq);
    //	console.log(freq);
    var result =  Math.abs(r);
    return result;
  }

  var to_cents = function(result, freq) {
    return Math.floor(1200 * Math.log(result / freq(note)) / Math.log(2));
  }

  var print_result = function(result) {
    if ( result > 20 && result <=50) {
      $('#demo').html('You are too far! Try again');
    }else if (result >10 && result <=20) {
      $('#demo').html('You are almost there !');
    }else if (result <=10) {
      $('#demo').html('Exact');
      // check();
    }
  }

  var check = function() {
    player.stop();
    guesser.stop();
  }

  var init = function() {
    $('#btn1').click(player_btn_clicked);
    $('#btn2').click(guesser_btn_clicked);
  };

  init();

  window.sineosc = sineosc;
  window.play_tone = play_tone;
  //window.buttonclick_result1=buttonclick_result1;
  //window.buttonclick_result2=buttonclick_result2;
  window.guess_tone = guess_tone;
  window.sineoscoff = sineoscoff;
  window.guess_up_clicked = guess_up_clicked;
  window.guess_down_clicked = guess_down_clicked;
  window.check = check;

})(window);
