var instrument_map={
	"1": "Santoor",
	"2": "Shehnai",
	"3": "Harmonium",
	"4": "Flute",
	"5": "Sitar"};
var generated_notes_map={
	"1": 1,
	"2": -1,
	"3": 7,
	"4": -7,
	"5": 12,
	"6": -12,
	"7": -5};
var note_array=["g2","gs2","a2","as2","b2","c3","cs3","d3","ds3","e3","f3","fs3","g3","gs3","a3","as3","b3","c4","cs4","d4","ds4","e4","f4","fs4","g4"];
var tone1,tone2;
var played1,played2;
var instrument;
var score=0;
var total=0;

function init()
{
	var instruments=document.getElementById("ins");
	instrument=instruments.options[instruments.selectedIndex].innerHTML.toLowerCase();
	generate();
	ML.collectRetentionAndSave(1);

}
function generate()
{
	played1=0;
	played2=0;
	var num1=Math.floor(Math.random()*25);
	tone1="/static/stimuli/instruments/"+instrument+"/"+note_array[num1]+".mp3";
	var rand=Math.floor(Math.random()*2);
	if(rand==0)
		tone2=tone1;
	else
	{
		var num2=Math.floor(Math.random()*7);
		while(!(num1+generated_notes_map[num2]>=0 && num1+generated_notes_map[num2]<25))
			num2=Math.floor(Math.random()*7);
		tone2="/static/stimuli/instruments/"+instrument+"/"+note_array[num1+generated_notes_map[num2]]+".mp3";
	}
}
function playsound1()
{
	var audioElement=document.createElement('audio');
	audioElement.setAttribute('src', tone1);
	audioElement.play();
	played1=1;
}
function playsound2()
{
	var audioElement=document.createElement('audio');
	audioElement.setAttribute('src', tone2);
	audioElement.play();
	played2=1;
}
function yes()
{
	if(played1==1 && played2==1)
	{
		if(tone1==tone2)
		{
			document.getElementById("div1-1").innerHTML="You are right!";
			total++;
			score++;
		}
		else
		{
			document.getElementById("div1-1").innerHTML="You are wrong.";
			total++;
		}
		document.getElementById("div1-2").innerHTML="Your score is "+score+" out of "+total;
		generate();
	}
}
function no()
{
	if(played1==1 && played2==1)
	{
		if(tone1==tone2)
		{
			document.getElementById("div1-1").innerHTML="You are wrong.";
			total++;
			ML.postScore(0, 1, 1);

		}
		else
		{
			document.getElementById("div1-1").innerHTML="You are right!";
			total++;
			score++;
			ML.postScore(1, 1, 1);

		}
		document.getElementById("div1-2").innerHTML="Your score is "+score+" out of "+total;
		generate();
	}
}
