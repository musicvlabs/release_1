(function(window){

var rand1,rand2;
var target1,target2;
var level=0;
var score=0;
var total=0;
var buttonpress=0;
var prevlevel=0;
var nodes;
var edges;
var dic={"taal":[],"bol":[],"matra":[],"jati":[],"noofmatras":[],"vibhaag":[],"taalbol":[]};
var matras,starting;
var lays=[2,3,4];
var mapping=['dugun','tigun','chaugun'];
var taalpattern;
var matrasperline;

$(document).ready(function()
		{
			ML.collectRetentionAndSave(14);

		$.ajax(
			{
type:"GET",
url:"/static/data/nodes1taal.csv",
dataType: "text",
success: function(data) { parsenodes(data); }
});
		$.ajax(
			{
type:"GET",
url: "/static/data/edges1taal.csv",
dataType: "text",
success: function(data) { parseedges(data); }
});
		});

function parsenodes(data)
{
	nodes=$.csv.toObjects(data);
	console.log("file parsed");
}

function parseedges(data)
{
	edges=$.csv.toObjects(data);
	console.log("edges parsed");
	distribute();
}

function distribute()
{
	for(i=1;i<97;i++)
	{
		x=nodes[i-1]["Type"].replace(/'/g, "");
		dic[x].push(nodes[i-1].Label);
	}
	for(i=21;i<45;i++)
	{
		dic["noofmatras"][edges[i].Source-1]=nodes[edges[i].Target-1]["Label"].replace(/'/g,"");
	}
	for(i=0;i<24;i++)
	{
		dic["vibhaag"][i]=new Array();
		dic["taalbol"][i]=new Array();
	}
	for(i=68;i<357;i++)
                dic['taalbol'][edges[i].Source-1].push(nodes[edges[i].Target-1].Label.replace(/'/g,""));
	for(i=357;i<453;i++)
                dic['vibhaag'][edges[i].Source-1].push(nodes[edges[i].Target-1].Label.replace(/'/g,""));
	console.log(dic);
	init();
}

function init()
{
	document.renderflag=1;
	$("#level-selector li a").click(function() {
	    	$('#level-selector li').removeClass("active");
            	$(this).parent().addClass("active");
	  	});
	$(".flip").click(function(e) {
		e.preventDefault();
		target1=$(this).attr("href");
		$(target1).slideToggle("fast");
		$(".panel").not(target1).hide();
		prevlevel=level;
		level=$('#level-selector li.active').val();
		if((level==1 && (prevlevel==2 || prevlevel==0)) || (level==2 && (prevlevel==1 || prevlevel==0)))
		{
			next();
		}
		});
}

function showanswer()
{
	buttonpress=1;
	if(matras[1]!=0)
		document.getElementById("answer").innerHTML="Number of matras: "+matras[0]+" "+matras[1]+"/"+matras[2]+", Starting position: "+starting[0]+" "+starting[1]+"/"+starting[2];
	else
		document.getElementById("answer").innerHTML="Number of matras: "+matras[0]+", Starting position: "+starting[0];
	document.getElementById("score").innerHTML="Score: "+score+"/"+total;
}

function next()
{
	document.getElementById("answer").innerHTML="";
	document.getElementById("rightorwrong").innerHTML="";
	document.getElementById("score").innerHTML="Score: "+score+"/"+total;
	if(level==1)
	{
		document.getElementsByName("matras")[0].value="";
	}
	else if(level==2)
	{
		document.getElementsByName("matras")[1].value="";
		document.getElementById("starting").value="";
	}
	generate();
}

function generate()
{
	$(".panel").not(target1).hide();
	rand1=Math.floor(Math.random()*24);
	var taalmatras=dic["noofmatras"][rand1];
	var taalname=dic["taal"][rand1];
	rand2=Math.floor(Math.random()*3);
	total++;
	buttonpress=0;
	matras=new Array(3);
	starting=new Array(3);
	matras[0]=Math.floor(taalmatras/lays[rand2]);
	matras[2]=lays[rand2];
	matras[1]=taalmatras%lays[rand2];
	if(matras[1]==0)
	{
		starting[1]=0;
		starting[0]=taalmatras-matras[0];
	}
	else
	{
		starting[1]=lays[rand2]-matras[1];
		starting[0]=taalmatras-matras[0]-1;
	}
	starting[2]=lays[rand2];
	var question=document.getElementsByName("ques");
	if(level==1)
		question[0].innerHTML="In how many matras is the "+mapping[rand2]+" of "+taalname+" done?";
	else if(level==2)
		question[1].innerHTML="In how many matras is the "+mapping[rand2]+" of "+taalname+" done and what is the starting position?";
	console.log(matras);
}

function submit()
{
	if(buttonpress==1)
		return;
	buttonpress=1;
	var flag=0;
	var alerts=document.getElementsByName("alert");
	for(i=0;i<alerts.length;i++)
		alerts[i].innerHTML="";
	var x=document.getElementsByName("matras");	//since there are 2 textboxes for matras - level 1 and level 2
	var y=document.getElementById("starting");
	var intpart,fraction,sintpart,sfraction;	//intpart - for the integer part of matras, fraction - for the fraction part of matras, sintpart - for the integer part of starting point, sfraction - for the fraction part of starting point
	if((level==1 && (x[0].value.localeCompare("")==0 || x[0].value.localeCompare(" ")==0)) || (level==2 && ((x[1].value.localeCompare("")==0 || x[1].value.localeCompare(" ")==0) || (y.value.localeCompare("")==0 || y.value.localeCompare(" ")==0))))
	{
		buttonpress=0;
		return;
	}
	if(level==1)
		intpart=x[0].value.split(" ");
	else if(level==2)
	{
		intpart=x[1].value.split(" ");
		sintpart=y.value.split(" ");
	}
	if((intpart.length==2 && (intpart[0].localeCompare("")==0 || intpart[1].localeCompare("")==0)) || intpart.length>2)
	{
		console.log(alerts[0]);
		if(level==1)
			alerts[0].innerHTML="Incorrect format";
		else if(level==2)
			alerts[1].innerHTML="Incorrect format";
		console.log('yes');
		buttonpress=0;
		return;
	}
	if(level==2 && ((sintpart.length==2 && (sintpart[0].localeCompare("")==0 || sintpart[1].localeCompare("")==0)) || sintpart.length>2))
	{
		alerts[2].innerHTML="Incorrect format";
		console.log('yes');
		buttonpress=0;
		return;
	}

	if(matras[1]==0)
	{
		if(intpart[0].localeCompare(matras[0])==0 && intpart.length==1)
		{
			if(level==2)
			{
				if(!(sintpart[0].localeCompare(starting[0])==0 && sintpart.length==1))
				{
					document.getElementById("rightorwrong").innerHTML="You are wrong.";
					document.getElementById("score").innerHTML="Score: "+score+"/"+total;
					ML.postScore(0, 14, 2);

					rendertaal();
					return;
				}
			}
			document.getElementById("rightorwrong").innerHTML="You are right!";
			ML.postScore(1, 14, 2);

			score++;
			document.getElementById("score").innerHTML="Score: "+score+"/"+total;
		}
		else
		{
			document.getElementById("rightorwrong").innerHTML="You are wrong.";
			document.getElementById("score").innerHTML="Score: "+score+"/"+total;
			ML.postScore(0, 14, 2);

		}
	}
	else
	{
		console.log(intpart[1]);
		if(intpart[1]==undefined || (level==2 && sintpart[1]==undefined))
		{
			document.getElementById("rightorwrong").innerHTML="You are wrong.";
			document.getElementById("score").innerHTML="Score: "+score+"/"+total;
			ML.postScore(0, 14, 2);

			rendertaal();
			return;
		}
		fraction=intpart[1].split("/");
		if(level==2)
			var sfraction=sintpart[1].split("/");
		console.log(fraction);
		if(((fraction[0].localeCompare(matras[1])==0 && fraction[1].localeCompare(matras[2])==0)) && intpart[0].localeCompare(matras[0]==0))
		{
			if(level==2 && !(((sfraction[0].localeCompare(starting[1])==0 && sfraction[1].localeCompare(starting[2])==0))) && sintpart[0].localeCompare(starting[0]==0))
			{
				document.getElementById("rightorwrong").innerHTML="You are wrong.";
				document.getElementById("score").innerHTML="Score: "+score+"/"+total;
				ML.postScore(0, 14, 1);

				rendertaal();
				return;
			}
			document.getElementById("rightorwrong").innerHTML="You are right!";
			ML.postScore(1, 14, 1);
			score++;
			document.getElementById("score").innerHTML="Score: "+score+"/"+total;
		}
		else
		{
			document.getElementById("rightorwrong").innerHTML="You are wrong.";
			document.getElementById("score").innerHTML="Score: "+score+"/"+total;
			ML.postScore(0, 14, 1);

		}
	}
	rendertaal();
}

function rendertaal()
{
	var count=0;
	taalpattern='tala pattern = |';
	var vibhaag=dic['vibhaag'][rand1];
	var vindex=0;
	var taalmatras=dic['noofmatras'][rand1];
	matrasperline='aksharas per line = '+taalmatras;
	console.log(rand1);
	console.log(vibhaag);
	for(i=0;i<taalmatras;i++)
	{
		if(i==0 || (vindex<vibhaag.length && i==vibhaag[vindex]-1))
		{
			taalpattern=taalpattern+'| ';
			if(i==vibhaag[vindex]-1)
			{
				vindex++;
			}
		}
		taalpattern=taalpattern+', ';
	}
	taalpattern=taalpattern+'||';
	var taalbol='';
	for(i=0;i<taalmatras;i++)
	{
		taalbol=taalbol+dic['taalbol'][rand1][i];
		if(i!=taalmatras-1)
			taalbol=taalbol+' ';
	}
	console.log(taalpattern);
	console.log(taalbol);
	var conditions='stretch = 1.25'+'\n'+'stretch space = 0.75';
	//var rendertext=document.getElementById("rendering");
	var extratext='&gt; Original speed'
	var rendertext=conditions+'\n\n'+taalpattern+'\n'+matrasperline+'\n\n'+extratext+'\n'+taalbol;
	console.log(rendertext);
	window.rendering(rendertext);
}

window.rendertaal = rendertaal;
window.init = init;
window.showanswer = showanswer;
window.submit = submit;
window.next = next;

})(window);
