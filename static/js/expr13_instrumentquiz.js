(function(window) {
var rand1,rand2;
var target1,target2;
var level=0,selected_category;
var instrument={
1: ['daf','dholak','khanjira','mridang','pakhawaj','tabla'],
2: ['ghatam','ghunghru','jaltarang','khartal','manjira','morchang'],
3: ['bansuri','harmonium','mukhveena','nadaswaram','pungi','shankh','shehnai'],
4: ['bulbultarang','ektara','rubab','rudraveena','santoor','saraswativeena','sarod','sitar','surmandal','tanpura'],
5: ['dilruba','esraj','ravanhatta','sarangi','violin']};
var dict={1:'avanadh',2:'ghan',3:'sushir',4:'tat',5:'vitat'};
var played=0;
var category,soundfile,instrumentname;
var score=0;
var total=0;
var selected=0;
var uls,ul;
var buttonpress=0;
var prevlevel=0;
var audioElement;

function init()
{
	ML.collectRetentionAndSave(13);

	$("#level-selector li a").click(function() {
	    	$('#level-selector li').removeClass("active");
            	$(this).parent().addClass("active");
	  	});
	$(".flip").click(function(e) {
		e.preventDefault();
		target1=$(this).attr("href");
		$(target1).slideToggle("fast");
		$(".panel").not(target1).hide();
		prevlevel=level;
		level=$('#level-selector li.active').val();
		if(played==1)
		{
			played=0;
			audioElement.pause();
		}
		if((level==1 && (prevlevel==2 || prevlevel==0)) || (level==2 && (prevlevel==1 || prevlevel==0)))
		{
			next();
		}
		});
	$("#category2 li a").click(function() {
		$('#category2 li').removeClass("active");
		$(this).parent().addClass("active");
		});
	$(".flip2").click(function(e) {
		e.preventDefault();
		target2=$(this).attr("href");
		$(target2).slideToggle("fast");
		$(".panel").not(target2).hide();
		$(target1).show();
		selected_category=$('#category2 li.active').val();
		uls=document.getElementsByName('instruments');
		ul=uls[selected_category-1];
		console.log(uls[selected_category-1].childNodes.length);
		if(uls[selected_category-1].childNodes.length==1)
		{
		for(var i=0;i<instrument[selected_category].length;i++)
		{
			var option=document.createElement('input');
			option.setAttribute('type','radio');
			option.setAttribute('name','ins');
			uls[selected_category-1].setAttribute('align','left');
			var label=document.createElement('label');
			label.innerHTML=instrument[selected_category][i];
			label.setAttribute('for',selected_category+''+(i+1));
			var enter=document.createElement('br');
			uls[selected_category-1].appendChild(option);
			uls[selected_category-1].appendChild(label);
			uls[selected_category-1].appendChild(enter);
			uls[selected_category-1].appendChild(enter);
		}}
		});
}

function showanswer()
{
	buttonpress=1;
	if(played==1)
	{
		audioElement.pause();
		played=0;
	}
	document.getElementById("answer").innerHTML="Category: "+category+", Instrument: "+instrumentname;
	document.getElementById("score").innerHTML="Score: "+score+"/"+total;
}

function next()
{
	if(played==1)
	{
		played=0;
		audioElement.pause();
	}
	document.getElementById("answer").innerHTML="";
	document.getElementById("rightorwrong").innerHTML="";
	document.getElementById("score").innerHTML="Score: "+score+"/"+total;
	if(level==1)
	{
		var cats=document.getElementsByName("cat"); //all radio buttons for level 1
		var flag=0;
		for(i=0;i<cats.length;i++)
		{
			if(cats[i].checked==true)
			{
				cats[i].checked=false;
				break;
			}
		}
	}
	else if(level==2)
	{
		if(uls!=undefined && selected_category!=undefined)
		{
			console.log(selected_category);
			for(var i=1;i<uls[selected_category-1].childNodes.length;i=i+3)
			{
				if(uls[selected_category-1].childNodes[i].checked==true)
				{
					uls[selected_category-1].childNodes[i].checked=false;
					break;
				}
			}

		}
	}
	generate();
}

function generate()
{
	played=0;
	selected=0;
	$(".panel").not(target1).hide();
	var rand=Math.floor(Math.random()*5);
	category=dict[rand+1];
	var rand2=Math.floor(Math.random()*instrument[rand+1].length);
	instrumentname=instrument[rand+1][rand2];
	var rand3=Math.floor(Math.random()*6)+1; //change this according to the number of sound files for each instrument
	soundfile='/static/instrumentID/'+category+'/'+instrumentname+'/'+rand3+'.mp3';
	console.log(category);
	console.log(instrumentname);
	total++;
	buttonpress=0;
	audioElement=undefined;
}

function submit()
{
	if(buttonpress==1)
		return;
	buttonpress=1;
	if(played==0)
		return;
	played=0;
	audioElement.pause();
	var flag=0;
	if(level==1)
	{
		var cats=document.getElementsByName("cat"); //all radio buttons for level 1
		var flag=0;
		for(i=0;i<cats.length;i++)
		{
			if(cats[i].checked==true)
			{
				flag=1;
				break;
			}
		}
		if(flag==0)
			return;
		if(category.localeCompare(cats[i].value)==0)
		{
			score++;
			document.getElementById("rightorwrong").innerHTML="You are right!";
			document.getElementById("score").innerHTML="Score: "+score+"/"+total;
			ML.postScore(1, 13, 1);

		}
		else
		{
			document.getElementById("rightorwrong").innerHTML="You are wrong.";
			document.getElementById("score").innerHTML="Score: "+score+"/"+total;
		}
		ML.postScore(0, 13, 1);

	}
	else if(level==2)
	{
		var i;
		for(i=1;i<uls[selected_category-1].childNodes.length;i=i+3)
		{
			if(uls[selected_category-1].childNodes[i].checked==true)
			{
				flag=1;
				break;
			}
		}
		if(flag==0)
			return;
		var selected_instrument=uls[selected_category-1].childNodes[i+1].innerHTML;
		if(instrumentname.localeCompare(selected_instrument)==0)
		{
			score++;
			document.getElementById("rightorwrong").innerHTML="You are right!";
			document.getElementById("score").innerHTML="Score: "+score+"/"+total;
			ML.postScore(1, 13, 1);

		}
		else
		{
			document.getElementById("rightorwrong").innerHTML="You are wrong.";
			document.getElementById("score").innerHTML="Score: "+score+"/"+total;
		}
		ML.postScore(0, 13, 2);

	}
}

function playtone_clicked()
{
	if(audioElement!=undefined && audioElement.ended==true)
		played=0;
	if(played==1)
		return;
	played=1;
	audioElement=document.createElement('audio');
	audioElement.setAttribute('src',soundfile);
	audioElement.play();
}

window.init=init;
window.playtone_clicked=playtone_clicked;
window.submit=submit;
window.next=next;
window.showanswer=showanswer;
})(window);
