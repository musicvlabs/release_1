var nodes;
var edges;
var score=0;
var total=0;
var pos;
var ans;
var dic={"raga":[],"thaat":[],"jati":[],"time":[],"svar":[],"pakad":[],"aaroh":[],"avroh":[]};
var raga_objects={};
var svar_map={'S':0,'r':1,'R':2,'g':3,'G':4,'m':5,'M':6,'P':7,'d':8,'D':9,'n':10,'N':11};
var svars=['S','r','R','g','G','m','M','P','d','D','n','N'];
var default_dictionary={'S':[],'r':[],'R':[],'g':[],'G':[],'m':[],'M':[],'P':[],'d':[],'D':[],'n':[],'N':[]};
var thaats_svars={"Bilaval":'SRGmPDN',"Kalyan":'SRGMPDN',"Khamaj":'SRGmPDn',"Bhairav":'SrGmPdN',"Poorvi":'SrGMPdN',"Marwa":'SrGMPDN',"Kafi":'SRgmPDn',"Asavari":'SRgmPdn',"Bhairavi":'SrgmPdn',"Todi":'SrgMPdN'}
var thaats_binary={};
var svar_variations={'S':['S'],'R':['r','R'],'G':['g','G'],'M':['m','M'],'P':['P'],'D':['d','D'],'N':['n','N']};
var all_permutations={};
var level;
var submit;
var shuddh_svars={'S':0,'R':1,'G':2,'M':3,'P':4,'D':5,'N':6};


String.prototype.replaceAt=function(index, character) 
{
	return this.substr(0, index) + character + this.substr(index+1,12);
}


$(document).ready(function()
		{
		$(".flip").click(function(e) {
			e.preventDefault();
			var target = $(this).attr("href");
			$(target).slideToggle("fast");
			console.log(target);
			$(".panel").not(target).hide();
			//var level=$('#level-selector li.active').val();
			level = target.split('panel')[1];
			console.log(level);
			$(target).slideDown("fast");
			return false;
			});
		ML.collectRetentionAndSave(6);

		$.ajax(
			{
type:"GET",
url:"/static/data/nodesold.csv",
dataType: "text",
success: function(data) { parsenodes(data); }
});
		$.ajax(
			{
type:"GET",
url: "/static/data/edgesold.csv",
dataType: "text",
success: function(data) { parseedges(data); }
});
		$.ajax(
			{
type:"GET",
url:"/static/js/permutations.json",
dataType: "json",
success: function(data) { store(data); }
});
		});


function parsenodes(data)
{
	console.log("opening file");
	nodes=$.csv.toObjects(data);
	console.log("file parsed");
}


function parseedges(data)
{
	console.log("opening file");
	edges=$.csv.toObjects(data);
	console.log("file parsed");
}


function store(data)
{
	all_permutations=data;
	distribute();
}


function distribute()
{
	for(i=1;i<386;i++)
	{
		x=nodes[i-1]["Type"].replace(/'/g, "");
		dic[x].push(nodes[i-1].Label.replace(/'/g,""));
	}
	for(i=0;i<163;i++)
	{
		raga_objects[dic['raga'][i]]={binary_aaroh:'000000000000',binary_avroh:'000000000000',binary_both:'000000000000'};
	}
	for(i=979;i<3750;i++)
	{
		var svar_name=nodes[edges[i-1].Target-1].Label.replace(/'/g, "");
		var raga_name=dic['raga'][edges[i-1].Source-1].replace(/'/g,"");
		if(svar_name=='')
			continue;
		raga_objects[raga_name].binary_aaroh=raga_objects[raga_name].binary_aaroh.replaceAt(svar_map[svar_name],'1');
		raga_objects[raga_name].binary_both=raga_objects[raga_name].binary_both.replaceAt(svar_map[svar_name],'1');
	}
	for(i=3750;i<7004;i++)
	{
		var svar_name=nodes[edges[i-1].Target-1].Label.replace(/'/g, "");
		var raga_name=dic['raga'][edges[i-1].Source-1].replace(/'/g,"");
		if(svar_name=='')
			continue;
		raga_objects[raga_name].binary_avroh=raga_objects[raga_name].binary_avroh.replaceAt(svar_map[svar_name],'1');
		raga_objects[raga_name].binary_both=raga_objects[raga_name].binary_both.replaceAt(svar_map[svar_name],'1');
	}
	var list_of_thaats=Object.keys(thaats_svars);
	for(i=0;i<list_of_thaats.length;i++)
	{
		thaats_binary[list_of_thaats[i]]='000000000000';
		for(j=0;j<thaats_svars[list_of_thaats[i]].length;j++)
		{
			thaats_binary[list_of_thaats[i]]=thaats_binary[list_of_thaats[i]].replaceAt(svar_map[thaats_svars[list_of_thaats[i]][j]],'1');
		}
	}
		//generate();
}


function generate_raga()
{
}

function generate_random()
{
	var alankaar=document.getElementById("alankaar");
	var final_sequence='';
	var group=Math.floor(Math.random()*3)+3;
	var num=Math.floor(Math.random()*(all_permutations[group.toString()].length));
	var sequence=all_permutations[group.toString()][num];
	var list_of_thaats=Object.keys(thaats_binary);
	var possible_thaats=new Array();
	var distances=new Array();
	var positions=new Array();
	for(i=0;i<group;i++)
	{
		distances.push(0);
	}
	for(i=0;i<list_of_thaats.length;i++)
	{
		all_present=1;
		for(j=0;j<group;j++)
		{
			if(thaats_binary[list_of_thaats[i]][svar_map[sequence[j]]]=='0')
			{
				all_present=0;
				break;
			}
		}
		if(all_present==1)
		{
			possible_thaats.push(list_of_thaats[i]);
		}
	}
	if(possible_thaats.length==0)
	{
		console.log('no possible thaat so generating again');
		generate_random();
	}
	else
	{
		var selected_thaat=possible_thaats[Math.floor(Math.random()*possible_thaats.length)];
		var first_svar=sequence[0];
		var count=1;
		var indices=[];
		var lowerflag=0;
		for(i=0;i<group;i++)
		{
			indices.push(thaats_svars[selected_thaat].indexOf(sequence[i]));
			positions.push(indices[i]);
		}
		final_sequence=final_sequence+'Aaroh:'+'<br>'+'<br>'+'| ';
		if(indices.indexOf(5)>-1)
		{
			for(j=0;j<group;j++)
			{
				if(indices[j]==5)
					positions[j]=distances[j]=-2;
				else if(indices[j]==6)
					positions[j]=distances[j]=-1;
			}
			if(indices.indexOf(4)>-1)
			{
				count-=2;
				for(j=0;j<group;j++)
					if(indices[j]==4)
						positions[j]=distances[j]=-3;
			}
			if(indices.indexOf(3)>-1)
			{
				count-=2;
				for(j=0;j<group;j++)
					if(indices[j]==3)
						positions[j]=distances[j]=-4;
			}
		}
		else if(indices.indexOf(6)>-1)
		{
			for(j=0;j<group;j++)
				if(indices[j]==6)
					positions[j]=distances[j]=-1;
			if(indices.indexOf(4)>-1)
			{
				count--;
				for(j=0;j<group;j++)
					if(indices[j]==4)
						positions[j]=distances[j]=-3;
			}
		}
		else
		{
			for(j=0;j<group;j++)
				positions[j]=distances[j]=indices[j];
		}
		for(j=0;j<group;j++)
		{
			if(distances[j]<0)
				final_sequence=final_sequence+sequence[j]+",";
			else if(distances[j]>6)
				final_sequence=final_sequence+sequence[j]+"'";
			else
				final_sequence=final_sequence+sequence[j];
		}
		final_sequence=final_sequence+' | ';
		var initial_count=count;
		console.log(positions);
		while(true)
		{
			for(j=0;j<group;j++)
			{
				sequence=sequence.replaceAt(j,thaats_svars[selected_thaat][((thaats_svars[selected_thaat].indexOf(sequence[j]))+1)%7]);
				distances[j]++;
			}
			for(j=0;j<group;j++)
			{
				if(distances[j]<0)
					final_sequence=final_sequence+sequence[j]+",";
				else if(distances[j]>6)
					final_sequence=final_sequence+sequence[j]+"'";
				else
					final_sequence=final_sequence+sequence[j];
			}
			final_sequence=final_sequence+' | ';
			count++;
			if(count%4==0)
				final_sequence=final_sequence+'<br>'+'| ';
			if(count>4 && sequence[group-1]==first_svar)
				break;
		}
		var avroh_svars=thaats_svars[selected_thaat].split("").reverse().join("");
		avroh_svars=avroh_svars[6]+avroh_svars.substr(0,6);
		var avroh_sequence='';
		for(i=0;i<group;i++)
			avroh_sequence=avroh_sequence+avroh_svars[indices[i]];
		final_sequence=final_sequence+'<br>'+'<br>'+'Avroh:'+'<br>'+'<br>'+'| ';
		for(j=0;j<group;j++)	
		{
			distances[j]=7-positions[j];	//this is done to get the exact reverse behaviour for avroh, without changing the relative positions. Do not change.
			if(distances[j]<0)
				final_sequence=final_sequence+avroh_sequence[j]+",";
			else if(distances[j]>6)
				final_sequence=final_sequence+avroh_sequence[j]+"'";
			else
				final_sequence=final_sequence+avroh_sequence[j];
		}
		console.log(distances);
		final_sequence=final_sequence+' | ';
		var total=count;
		while(count>initial_count)
		{
			for(j=0;j<group;j++)
			{
				avroh_sequence=avroh_sequence.replaceAt(j,avroh_svars[((avroh_svars.indexOf(avroh_sequence[j]))+1)%7]);
				distances[j]--;
			}
			for(j=0;j<group;j++)	
			{
				if(distances[j]<0)
					final_sequence=final_sequence+avroh_sequence[j]+",";
				else if(distances[j]>6)
					final_sequence=final_sequence+avroh_sequence[j]+"'";
				else
					final_sequence=final_sequence+avroh_sequence[j];
			}
			final_sequence=final_sequence+' | ';
			count--;
			if((total-count+1)%4==0)
				final_sequence=final_sequence+'<br>'+'| '
		}
		final_sequence=final_sequence+'<br>'+'<br>'+'Thaat: '+selected_thaat;
		alankaar.innerHTML=final_sequence;

	}
}

function convert_to_svar(binary)
{
	var sequence='';
	var i;
	for(i=0;i<binary.length;i++)
		if(binary[i]=='1')
			sequence=sequence+svars[i]+' ';
	return sequence;
}
