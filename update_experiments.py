from classes import Experiment
from server import create_app, exp_num_file_map

app = create_app()

with app.app_context():
    for n in exp_num_file_map:
        print n, exp_num_file_map[n]
        exp = Experiment.query.get(int(n))
        exp.name = exp_num_file_map[n]
        exp.filename = 'expr' + exp.name + '.html'
        print exp.id, exp.name, exp.filename
        exp.persist()
