function(window){

var table = [0, [1, 100], [0.6, 100], [0, 1000]];

var freqlist = [440, 466, 493, 523, 554, 622, 659, 698, 739, 783, 830, 880];

var freq = freqlist[Math.floor(Math.random()*freqlist.length)];
var freq2 = Math.floor(Math.random()*((freq+50) - (freq-50)) + (freq-50));


function(player){
var synth = T("sin", {freq:freq, mul:0.25});
var synth = T("saw", {freq:freq, mul:0.25});
var synth = T("square", {freq:freq/2, mul:0.25});

var env   = T("env", {table:table, releaseNode:3}, synth).on("ended", function() {
  this.pause();
}).bang().play();

env.plot({target:releaseNode});

var timeout = T("timeout", {timeout:2000}, function() {
  env.release();
  timeout.stop();
}).start();
}


function(guesser){
  if (guessON == true){

  var freq2 = freqlist[Math.floor(Math.random()*freqlist.length)];
  var synth = T("sin", {freq:freq, mul:0.25});
  var synth = T("saw", {freq:freq, mul:0.25});
  var synth = T("square", {freq:freq/2, mul:0.25});

  var env   = T("env", {table:table, releaseNode:3}, synth).on("ended", function() {
    this.pause();
  }).bang().play();

  env.plot({target:releaseNode});

  var timeout = T("timeout", {timeout:2000}, function() {
    env.release();
    timeout.stop();
  }).start();
  }
  else{

  }

}



var compare = function(guess_freq, freq) {
  var r = guess_freq - freq;
  var result =  Math.abs(r);
  return result;
}


  var print_result = function(result) {
    if ( result > 20 && result <=50) {
      $('#demo').html('You are too far! Try again');
    }else if (result >10 && result <=20) {
      $('#demo').html('You are almost there !');
    }else if (result <=10) {
      $('#demo').html('Exact');
    }
  }


};
